﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Scorecard
{
    public class FixtureHelper
    {
        private int? _id;
        private Innings _first;
        private Innings _second;
        public DateTime Date { get; set; }
        public Team Home { get; set; }
        public Team Away { get; set; }
        public string Competition { get; set; }
        public string Round { get; set; }
        public Innings FirstInnings
        {
            get { return _first; }
            set 
            {
                _first = value;
                SetInnings();
            }
        }
        public Innings SecondInnings
        {
            get { return _second; }
            set
            {
                _second = value;
                SetInnings();
            }
        }
        public int Id
        {
            get
            {
                if (_id == null)
                {
                    _id = FindorCreateFixture();
                }
                return Convert.ToInt32(_id);
            }
        }

        public Team FindTeam(string name)
        {
            return Home.Club.Name.Equals(name) ? Home : Away;
        }

        private int FindorCreateFixture()
        {
            if (Home == null){throw new Exception("Unable to find fixture as Home Club was not supplied");}
            if (Away == null) { throw new Exception("Unable to find fixture as Away Club was not supplied"); }

            int result;
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "FindorCreateFixture";

                cmd.Parameters.AddWithValue("@Date", Date);
                cmd.Parameters.AddWithValue("@Competition", FindorCreateCompetition(Competition));
                cmd.Parameters.AddWithValue("@HomeTeam", Home.Id);
                cmd.Parameters.AddWithValue("@AwayTeam", Away.Id);
                cmd.Parameters.AddWithValue("@Round", FindorCreateRound(Round));
                var fixtureId = cmd.Parameters.Add("@FixtureId", SqlDbType.Int);
                fixtureId.Direction = ParameterDirection.Output;
                conn.Open();
                cmd.ExecuteScalar();
                result = (int)cmd.Parameters["@FixtureId"].Value;
                conn.Close();
            }

            return result;
        }
        private static int? FindorCreateCompetition(string competition)
        {
            if (competition != null)
            {
                int result;
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
                {
                    var cmd = conn.CreateCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "FindorCreateCompetition";

                    cmd.Parameters.AddWithValue("@Name", competition);
                    var competitionId = cmd.Parameters.Add("@CompetitionId", SqlDbType.Int);
                    competitionId.Direction = ParameterDirection.Output;
                    conn.Open();
                    cmd.ExecuteScalar();
                    result = (int)cmd.Parameters["@CompetitionId"].Value;
                    conn.Close();
                }

                return result;
            }
            return null;
        }

        private static int? FindorCreateRound(string round)
        {
            int? result = null;
            if (round != null)
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
                {
                    var cmd = conn.CreateCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "FindorCreateCompetitionRound";

                    cmd.Parameters.AddWithValue("@Name", round);
                    var competitionRoundId = cmd.Parameters.Add("@CompetitionRoundId", SqlDbType.Int);
                    competitionRoundId.Direction = ParameterDirection.Output;
                    conn.Open();
                    cmd.ExecuteScalar();
                    result = (int)cmd.Parameters["@CompetitionRoundId"].Value;
                    conn.Close();
                }
            }

            return result;
        }
        public void SetInnings()
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "UpdateFixture";

                cmd.Parameters.AddWithValue("@ID", Id);
                if (FirstInnings != null)
                {
                    cmd.Parameters.AddWithValue("@FirstInnings", FirstInnings.Id);
                }

                if (SecondInnings != null)
                {
                    cmd.Parameters.AddWithValue("@SecondInnings", SecondInnings.Id);
                }

                conn.Open();
                cmd.ExecuteScalar();
                conn.Close();
            }
        }
    }

    public class Innings
    {
        private int? _id;
        private List<Batter> _batters;
        private List<Bowler> _bowlers;
        private List<Dismissal> _dismissals; 

        public int Extras { get; set; }
        public Team Team { get; set; }
        public Player Captain { get; set; }
        public Player WicketKeeper { get; set; }
        public int Wides { get; set; }
        public int NoBalls { get; set; }
        public int Byes { get; set; }
        public int LegByes { get; set; }
        public int Id
        {
            get
            {
                if (_id == null)
                {
                    _id = CreateInnings();
                }
                return Convert.ToInt32(_id);
            }
        }

        public List<Batter> Batters 
        {
            get { return _batters ?? (_batters = new List<Batter>()); }
            set { _batters = value; }
        }
        public List<Bowler> Bowlers
        {
            get { return _bowlers ?? (_bowlers = new List<Bowler>()); }
            set { _bowlers = value; }
        }
        public List<Dismissal> Dismissals
        {
            get { return _dismissals ?? (_dismissals = new List<Dismissal>()); }
            set { _dismissals = value; }
        }
        private int CreateInnings()
        {
            if (Team == null) { throw new NullReferenceException("unable to create Innings as Team is null"); }

            int result;
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "CreateInnings";

                cmd.Parameters.AddWithValue("@Extras", Extras);
                cmd.Parameters.AddWithValue("@Team", Team.Id);
                if (Captain != null)
                {
                    cmd.Parameters.AddWithValue("@Captain", Captain.Id);
                }
                if (WicketKeeper != null)
                {
                    cmd.Parameters.AddWithValue("@WicketKeeper", WicketKeeper.Id);
                }
                cmd.Parameters.AddWithValue("@Wides", Wides);
                cmd.Parameters.AddWithValue("@NoBalls", NoBalls);
                cmd.Parameters.AddWithValue("@Byes", Byes);
                cmd.Parameters.AddWithValue("@LegByes", LegByes);
                var inningsId = cmd.Parameters.Add("@ID", SqlDbType.Int);
                inningsId.Direction = ParameterDirection.Output;
                conn.Open();
                cmd.ExecuteScalar();
                result = (int)cmd.Parameters["@ID"].Value;
                conn.Close();
            }

            return result;
        }
    }

    public class Dismissal
    {
        private int? _id;
        public Innings Innings { get; set; }
        public Player Batter { get; set; }
        public Player Bowler { get; set; }
        public Player Fielder { get; set; }
        public string DismissalCode { get; set; }
        public int Id
        {
            get
            {
                if (_id == null)
                {
                    _id = CreateDismissal();
                }
                return Convert.ToInt32(_id);
            }
        }

        private int CreateDismissal()
        {
            if (Innings == null) { throw new NullReferenceException("Innings is null"); }
            if (Batter == null) { throw new NullReferenceException("Batter is null"); }
            int result;
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "CreateDismissal";

                cmd.Parameters.AddWithValue("@Innings", Innings.Id);
                cmd.Parameters.AddWithValue("@Batter", Batter.Id);
                if (Bowler != null)
                {
                    cmd.Parameters.AddWithValue("@Bowler", Bowler.Id);
                }
                if (Fielder != null)
                {
                    cmd.Parameters.AddWithValue("@Fielder", Fielder.Id);
                }
                cmd.Parameters.AddWithValue("@Type", FindDismissalType());
                var dismissalId = cmd.Parameters.Add("@DismissalId", SqlDbType.Int);
                dismissalId.Direction = ParameterDirection.Output;
                conn.Open();
                cmd.ExecuteScalar();
                result = (int)cmd.Parameters["@DismissalId"].Value;
                conn.Close();
            }

            return result;
        }
        private int FindDismissalType()
        {
            if (string.IsNullOrEmpty(DismissalCode)){throw new NullReferenceException("unable to return type as dimissal code is null");}
            int result;
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetDismissalType";

                cmd.Parameters.AddWithValue("@DismissalCode", DismissalCode);
                var clubId = cmd.Parameters.Add("@DismissalId", SqlDbType.Int);
                clubId.Direction = ParameterDirection.Output;
                conn.Open();
                cmd.ExecuteScalar();
                result = (int)cmd.Parameters["@DismissalId"].Value;
                conn.Close();
            }

            return result;
        }
    }

    public class Batter
    {
        private int? _id;
        public Innings Innings { get; set; }
        public Player Player { get; set; }
        public int Position { get; set; }
        public int Score { get; set; }
        public int Id
        {
            get
            {
                if (_id == null)
                {
                    _id = FindorCreateBatter();
                }
                return Convert.ToInt32(_id);
            }
        }

        private int FindorCreateBatter()
        {
            if (Innings == null){throw new NullReferenceException("Innings is null");}
            if (Player == null) { throw new NullReferenceException("Player is null"); }
            int result;
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "FindorCreateBatter";

                cmd.Parameters.AddWithValue("@Innings", Innings.Id);
                cmd.Parameters.AddWithValue("@Player", Player.Id);
                cmd.Parameters.AddWithValue("@Position", Position);
                cmd.Parameters.AddWithValue("@Score", Score);
                var batterId = cmd.Parameters.Add("@BatterId", SqlDbType.Int);
                batterId.Direction = ParameterDirection.Output;
                conn.Open();
                cmd.ExecuteScalar();
                result = (int)cmd.Parameters["@BatterId"].Value;
                conn.Close();
            }

            return result;
        }
    }

    public class Bowler
    {
        private int? _id;
        public Innings Innings { get; set; }
        public Player Player { get; set; }
        public int Position { get; set; }
        public decimal Overs { get; set; }
        public int Maidens { get; set; }
        public int Runs { get; set; }
        public int Id
        {
            get
            {
                if (_id == null)
                {
                    _id = FindorCreateBowler();
                }
                return Convert.ToInt32(_id);
            }
        }

        private int FindorCreateBowler()
        {
            if (Innings == null) { throw new NullReferenceException("Innings is null"); }
            if (Player == null) { throw new NullReferenceException("Player is null"); }
            int result;
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "FindorCreateBowler";

                cmd.Parameters.AddWithValue("@Innings", Innings.Id);
                cmd.Parameters.AddWithValue("@Player", Player.Id);
                cmd.Parameters.AddWithValue("@Position", Position);
                cmd.Parameters.AddWithValue("@Overs", Overs);
                cmd.Parameters.AddWithValue("@Maidens", Maidens);
                cmd.Parameters.AddWithValue("@Runs", Runs);
                var bowlerId = cmd.Parameters.Add("@BowlerId", SqlDbType.Int);
                bowlerId.Direction = ParameterDirection.Output;
                conn.Open();
                cmd.ExecuteScalar();
                result = (int)cmd.Parameters["@BowlerId"].Value;
                conn.Close();
            }

            return result;
        }
    }

    public class Player
    {
        private int? _id;
        public string Surname { get; set; }
        public string Initials { get; set; }
        public Team Team { get; set; }
        public int Id
        {
            get
            {
                if (_id == null)
                {
                    _id = FindorCreatePlayer();
                }
                return Convert.ToInt32(_id);
            }
        }

        private int FindorCreatePlayer()
        {
            if (!String.IsNullOrEmpty(Surname))
            {
                int result;
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
                {
                    var cmd = conn.CreateCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "FindorCreatePlayer";

                    cmd.Parameters.AddWithValue("@Surname", Surname);
                    cmd.Parameters.AddWithValue("@Initials", Initials);
                    cmd.Parameters.AddWithValue("@TeamId", Team.Id);
                    var playerId = cmd.Parameters.Add("@PlayerId", SqlDbType.Int);
                    playerId.Direction = ParameterDirection.Output;
                    conn.Open();
                    cmd.ExecuteScalar();
                    result = (int)cmd.Parameters["@PlayerId"].Value;
                    conn.Close();
                }

                return result;
            }
            throw new NullReferenceException("player surname name is null");
        }
    }

    public class Team
    {
        private int? _id;
        public string Name { get; set; }
        public Club Club { get; set; }
        public int Id
        {
            get
            {
                if (_id == null)
                {
                    _id = FindorCreateTeam();
                }
                return Convert.ToInt32(_id);
            }
        }

        private int? FindorCreateTeam()
        {
            int? result = null;
            if (Club != null)
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
                {
                    var cmd = conn.CreateCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "FindorCreateTeam";

                    cmd.Parameters.AddWithValue("@Name", Name);
                    cmd.Parameters.AddWithValue("@Club", Club.Id);
                    var teamId = cmd.Parameters.Add("@TeamId", SqlDbType.Int);
                    teamId.Direction = ParameterDirection.Output;
                    conn.Open();
                    cmd.ExecuteScalar();
                    result = (int)cmd.Parameters["@teamId"].Value;
                    conn.Close();
                }
            }
            return result;
        }
    }

    public class Club
    {
        private int? _id;
        public string Name { get; set; }
        public int Id
        {
            get
            {
                if (_id == null)
                {
                    _id = FindorCreateClub();
                }

                return Convert.ToInt32(_id);
            }
        }

        private int FindorCreateClub()
        {
            if (!String.IsNullOrEmpty(Name))
            {
                int result;
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
                {
                    var cmd = conn.CreateCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "FindorCreateClub";

                    cmd.Parameters.AddWithValue("@Name", Name);
                    var clubId = cmd.Parameters.Add("@ClubId", SqlDbType.Int);
                    clubId.Direction = ParameterDirection.Output;
                    conn.Open();
                    cmd.ExecuteScalar();
                    result = (int)cmd.Parameters["@ClubId"].Value;
                    conn.Close();
                }

                return result;
            }
            throw new NullReferenceException("club name is null");
        } 
    }
}
