﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Text;
using System.Web;
using Newtonsoft.Json;

namespace Scorecard
{
    public partial class PasswordResetRequest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["email"]))
            {
                if (SendPasswordReset(HttpUtility.UrlDecode(Request.QueryString["email"])))
                {
                    ltrResult.Text = "<p class=\"tk-ff-enzo-web\">An email has been sent to your account with details of how to reset your password.</p>";
                }
                else
                {
                    Response.Redirect("/Default.aspx");
                }
            }
            else
            {
                Response.Redirect("/Default.aspx");
            }
        }

        private static bool SendPasswordReset(string email)
        {
            var result = false;
            var token = Guid.NewGuid();
            if (CreateReset(email, token) > -1)
            {
                SendEmail(email, token.ToString());
                result = true;
            }

            return result;
        }

        private static int CreateReset(string email, Guid token)
        {
            int result;
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "CreateReset";

                cmd.Parameters.AddWithValue("@Email", email);
                cmd.Parameters.AddWithValue("@Token", token.ToString());
                var accountid = cmd.Parameters.Add("@ID", SqlDbType.Int);
                accountid.Direction = ParameterDirection.Output;
                conn.Open();
                cmd.ExecuteScalar();
                result = Convert.ToInt32(cmd.Parameters["@ID"].Value);
                conn.Close();
            }
            return result;
        }

        private static void SendEmail(string email, string token)
        {
            var httpWReq = (HttpWebRequest)WebRequest.Create("http://api.postmarkapp.com/email");

            var encoding = new ASCIIEncoding();
            var emailbody =
                "To reset your Shepley CC password please click this link:\n\nhttp://www.shepleycc.com/PasswordReset/" + token;
            var postdata = new Dictionary<string, string>
                {
                    {"From", "jamie.dearnley@shepleycc.com"},
                    {"To", email},
                    {"Subject", "Shepley CC password reset request"},
                    {"Tag", "Registration"},
                    {"TextBody", emailbody},
                    {"ReplyTo", "jamie.dearnley@shepleycc.com"}
                };
            byte[] data = encoding.GetBytes(JsonConvert.SerializeObject(postdata));

            httpWReq.Method = "POST";
            httpWReq.Headers = new WebHeaderCollection { "X-Postmark-Server-Token: aa0f7d43-6878-4326-9920-ecfc53764bdf" };
            httpWReq.ContentType = "application/json";
            httpWReq.ContentLength = data.Length;

            using (var newStream = httpWReq.GetRequestStream())
            {
                newStream.Write(data, 0, data.Length);
            }

        }
    }
}