﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportAdandonedResult.aspx.cs" Inherits="Scorecard.ImportAdandonedResult" %>
<%@ Register TagPrefix="Nav" TagName="Navigation" Src="~/Controls/Navigation.ascx" %>
<%@ Register TagPrefix="Ftr" TagName="Footer" Src="~/Controls/Footer.ascx" %>
<%@ Register TagPrefix="Auth" TagName="Authentication" Src="~/Controls/Authentication.ascx" %>

<!DOCTYPE html>
<html lang="en">
 <head>
  <meta charset="utf-8" />
  <meta name="description" content="Shepley cricket club" />
  <meta name="author" content="Jamie Dearnley" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0" />
  <link rel="stylesheet" media="screen, projection" href="/styles/screen.css" />
  <title>Shepley Cricket Club - Import Abandoned Result</title>
  
  <!-- enable HTML5 elements in IE7+8 -->
  <!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <script type="text/javascript" src="//use.typekit.net/mdu4awa.js"></script>
  <script type="text/javascript">      try { Typekit.load(); } catch (e) { }</script>
  <script type="text/javascript" src="/scripts/jquery-1.8.3.min.js"></script>
 </head>
 <body>
  <div id="container">
   <form id="form1" runat="server">
<Nav:Navigation runat="server" ID="pageNav" />
   <br />
    <div id="importresults">
     <Auth:Authentication runat="server" Permissions="Import" ID="auth1" />
     
     <h2>Import Abandoned Result</h2>
     <fieldset>
      <ol>
       <li>To mark a fixture as abandonded you must select a team, year and fixture. If you cannot find the fixture you wish to update please contact the system administrator.<br /><br /></li>
       <li>To import a result you must also provide your account details. If you do not have an account you will be asked to register after entering your details.<br /><br /></li>
       <li>
        <asp:Label ID="Label1" runat="server" Text="First select a team"></asp:Label>
        <asp:DropDownList ID="dlTeams" runat="server" DataSourceID="sqlTeams" 
               DataTextField="Name" DataValueField="ID" 
               onselectedindexchanged="dlTeams_SelectedIndexChanged" AutoPostBack="True" 
               ondatabound="dlTeams_DataBound" ></asp:DropDownList>
        <asp:SqlDataSource ID="sqlTeams" runat="server" ConnectionString="<%$ ConnectionStrings:Scorecard %>" SelectCommand="GetTeams" SelectCommandType="StoredProcedure">
         <SelectParameters>
          <asp:Parameter DefaultValue="<%$ AppSettings:Club %>" Name="Club" Type="String" />
         </SelectParameters>
        </asp:SqlDataSource><br />
       </li>
       <li>
        <asp:Label ID="lblYears" runat="server" Text="Then select a year"></asp:Label>
        <asp:DropDownList ID="dlYears" runat="server" onselectedindexchanged="dlYears_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
       </li>
       <li>
        <asp:Label ID="lblFixtures" runat="server" Text="Finally select a fixture"></asp:Label>
        <asp:DropDownList ID="dlFixtures" runat="server"></asp:DropDownList>
       </li>
       <li>
        <asp:Button ID="btnImport" runat="server" Text="Mark as Abandoned" onclick="btnAbandoned_Click" />
        <asp:Button ID="btnHomeConcede" runat="server" Text="Mark as Conceded by Home team" onclick="btnHomeConceded_Click" />
        <asp:Button ID="btnAwayConcede" runat="server" Text="Mark as Conceded by Away team" onclick="btnAwayConceded_Click" />
       </li>
      </ol>
     </fieldset>
        
      <asp:Literal ID="ltrDebug" runat="server"></asp:Literal>
     </div>
    </form>
   
   <div class="clearfooter"></div>
  </div>
  
<Ftr:Footer runat="server" ID="pageFooter" />
 </body>
</html>