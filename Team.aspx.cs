﻿using System;
using System.Web.UI;

namespace Scorecard
{
    public partial class Team1 : Page
    {
        private DateTime? _year;
        private string _team;

        public string Team {
            get
            {
                if (string.IsNullOrEmpty(_team))
                {
                    _team = "A";
                }
                return _team;
            }
            set { _team = value; }
        }
        public DateTime Year
        {
            get
            {
                try
                {
                    if (_year == null)
                    {
                        _year = DateTime.Now;
                    }
                    return Convert.ToDateTime(_year);
                }
                catch (Exception)
                {
                    return DateTime.Now;
                }
            }
            set { _year = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["Team"]))
            {
                Team = Request.QueryString["Team"];
            }

            if (!string.IsNullOrEmpty(Request.QueryString["Year"]))
            {
                DateTime temp;
                if (DateTime.TryParse(Request.QueryString["Year"] + "/01/01", out temp))
                {
                    Year = temp;
                }
            }

            teamFixtures.Year = Year;
            teamFixtures.Team = Team;
            teamAverages.Year = Year;
            teamAverages.Team = Team;
        }
    }
}