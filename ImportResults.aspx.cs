﻿using System;
using System.Collections.Generic;
using System.Globalization;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

namespace Scorecard
{
    public partial class ImportResults : System.Web.UI.Page
    {
        protected void btnImport_Click(object sender, EventArgs e)
        {
            if (auth1.Authenticate())
            {
                if (FileUpload1.HasFile)
                {
                    try
                    {

                        var wkk = new HSSFWorkbook(FileUpload1.FileContent);
                        ISheet sheet = wkk.GetSheetAt(0);
                        var fixture = GetFixture(sheet);
                        if (fixture.Competition == null)
                        {
                            fixture.Competition = dlCompetitions.SelectedItem.Text;
                        }

                        if (fixture.Round == null && dlRounds.SelectedIndex != 0)
                        {
                            fixture.Round = dlRounds.SelectedItem.Text;
                        }

                        var fiteam = sheet.GetRow(1).GetCell(1).ToString();
                        var firstinnings = new Innings
                        {
                            Team = fixture.FindTeam(fiteam.Substring(0, fiteam.LastIndexOf(' '))),
                            Extras = Convert.ToInt32(sheet.GetRow(14).GetCell(6).ToString()),
                        };
                        firstinnings.Batters = CreateBatters(firstinnings, sheet, 3, 13);

                        var siteam = sheet.GetRow(16).GetCell(1).ToString();
                        var secondinnings = new Innings
                        {
                            Team = fixture.FindTeam(siteam.Substring(0, siteam.LastIndexOf(' '))),
                            Extras = Convert.ToInt32(sheet.GetRow(29).GetCell(6).ToString()),
                        };
                        secondinnings.Batters = CreateBatters(secondinnings, sheet, 18, 28);

                        firstinnings.Bowlers = CreateBowlers(firstinnings, sheet, 18, 28, secondinnings.Team);
                        secondinnings.Bowlers = CreateBowlers(secondinnings, sheet, 3, 13, firstinnings.Team);

                        firstinnings.Dismissals = CreateDismissals(firstinnings, sheet, 3, 13, secondinnings.Team);
                        secondinnings.Dismissals = CreateDismissals(secondinnings, sheet, 18, 28, firstinnings.Team);

                        fixture.FirstInnings = firstinnings;
                        fixture.SecondInnings = secondinnings;
                    }
                    catch (Exception error)
                    {
                        ltrDebug.Text = error.Message;
                    }
                }
                else
                {
                    ltrDebug.Text = "You must select an excel file to import";
                }
            }
        }

        private static List<Dismissal> CreateDismissals(Innings innings, ISheet wk, int startrow, int endrow, Team bowlerTeam)
        {
            var result = new List<Dismissal>();
            var count = startrow;
            var processing = true;

            while (count <= endrow && processing)
            {
                var position = wk.GetRow(count).GetCell(0);
                if (position != null && !string.IsNullOrEmpty(position.ToString()))
                {
                    if (!string.IsNullOrEmpty(wk.GetRow(count).GetCell(1).ToString()))
                    {
                        var batter = wk.GetRow(count).GetCell(1).ToString().Replace('.', ' ').Split(new[] { ' ' }, 2);
                        var bowler = wk.GetRow(count).GetCell(5);
                        var fielder = wk.GetRow(count).GetCell(3);
                        var type = wk.GetRow(count).GetCell(2).ToString();
                        if (string.IsNullOrEmpty(type))
                        {
                            type = "d";
                        }
                        if (type.Equals("x-retired"))
                        {
                            type = "z";
                        }
                        var dismissal = new Dismissal
                        {
                            Batter = innings.Batters.Find(b => b.Player.Surname.Equals(batter[1]) && b.Player.Initials.Equals(batter[0])).Player,
                            DismissalCode = type,
                            Innings = innings
                        };
                        if (bowler != null && !string.IsNullOrEmpty(bowler.ToString()))
                        {
                            var name = bowler.ToString().Replace('.', ' ').Split(new[] { ' ' }, 2);
                            dismissal.Bowler = innings.Bowlers.Find(b => b.Player.Surname.Equals(name[1]) && b.Player.Initials.Equals(name[0])).Player;
                        }
                        if (fielder != null && !string.IsNullOrEmpty(fielder.ToString()))
                        {
                            dismissal.Fielder = SetFielder(bowlerTeam, fielder);
                        }

                        var id = dismissal.Id;
                        result.Add(dismissal);
                    }
                    
                    count++;
                }
                else
                {
                    processing = false;
                }
            }
            return result;
        }
        private static Player SetFielder(Team bowlerTeam, object fielder)
        {
            var name = fielder.ToString().Replace('.', ' ').Split(new[] { ' ' }, 2);
            Player result;
            
            if (name.Length.Equals(2))
            {
                result = new Player
                {
                    Initials = name[0],
                    Surname = name[1],
                    Team = bowlerTeam
                };
            }
            else
            {
                result = new Player
                {
                    Initials = "",
                    Surname = name[0],
                    Team = bowlerTeam
                };
            }
            return result;
        }
        private static List<Bowler> CreateBowlers(Innings innings, ISheet wk, int startrow, int endrow, Team bowlerTeam)
        {
            var result = new List<Bowler>();
            var count = startrow;
            var processing = true;

            while (count <= endrow && processing)
            {
                var player = wk.GetRow(count).GetCell(1);
                if (player != null && !string.IsNullOrEmpty(player.ToString()))
                {
                    var name = player.ToString().Replace('.', ' ').Split(new[] { ' ' }, 2);
                    var overs = wk.GetRow(count).GetCell(7);
                    var position = wk.GetRow(count).GetCell(11);
                    var pos = count;
                    if (position != null && !string.IsNullOrEmpty((position.ToString())))
                    {
                        pos = Convert.ToInt32(position.ToString());
                    }
                    if (overs != null && !string.IsNullOrEmpty(overs.ToString()))
                    {
                        
                        var bowler = new Bowler
                            {
                                Innings = innings,
                                Player = new Player
                                    {
                                        Initials = name[0],
                                        Surname = name[1],
                                        Team = bowlerTeam
                                    },
                                Overs = Convert.ToDecimal(overs.ToString()),
                                Maidens = Convert.ToInt32(wk.GetRow(count).GetCell(8).ToString()),
                                Runs = Convert.ToInt32(wk.GetRow(count).GetCell(9).ToString()),
                                Position = pos
                            };
                        var id = bowler.Id;
                        result.Add(bowler);
                    }
                    count++;
                }
                else
                {
                    processing = false;
                }
            }
            return result;
        }
        private static List<Batter> CreateBatters(Innings innings, ISheet wk, int startrow, int endrow)
        {
            var result = new List<Batter>();
            var count = startrow;
            var processing = true;

            while (count <= endrow && processing)
            {
                var position = wk.GetRow(count).GetCell(0);
                if (position != null && !string.IsNullOrEmpty(position.ToString()))
                {
                    if (!string.IsNullOrEmpty(wk.GetRow(count).GetCell(1).ToString()))
                    {
                        var name = wk.GetRow(count).GetCell(1).ToString().Replace('.', ' ').Split(new[] { ' ' }, 2);
                        var batter = new Batter
                        {
                            Innings = innings,
                            Player = new Player { Team = innings.Team, Initials = name[0], Surname = name[1] },
                            Position = Convert.ToInt32(position.ToString())
                        };

                        var score = wk.GetRow(count).GetCell(6);
                        if (score != null && !string.IsNullOrEmpty(score.ToString()))
                        {
                            batter.Score = Convert.ToInt32(score.ToString());
                        }
                        var id = batter.Id;
                        result.Add(batter);
                    }

                    count++;
                }
                else
                {
                    processing = false;
                }
            }

            return result;
        }

        private static DateTime ParseDate(string date)
        {
            DateTime result;

                DateTime.TryParseExact(date, "M'/'d'/'yy", CultureInfo.InvariantCulture, DateTimeStyles.None, out result);
                return result;
        }

        private static FixtureHelper GetFixture(ISheet wk)
        {
            var date = ParseDate(wk.GetRow(0).GetCell(0).ToString()); //wk.Cells[1, 1].Value.ToString();
            var home = wk.GetRow(0).GetCell(3).ToString(); //wk.Cells[1, 4].Value.ToString();
            var away = wk.GetRow(0).GetCell(5).ToString(); //wk.Cells[1, 6].Value.ToString();

            if (date.Hour.Equals(0))
            {
                date = date.AddHours(14);
            }

            var fixture = new FixtureHelper
                {
                    Home = new Team { Club = new Club { Name = home.Substring(0, home.LastIndexOf(' ')) }, Name = home.Substring(home.LastIndexOf(' ') + 1) },
                    Away = new Team { Club = new Club { Name = away.Substring(0, away.LastIndexOf(' ')) }, Name = away.Substring(away.LastIndexOf(' ') + 1) },
                    Date = date,
                };

            

            return fixture;
        }
    }
}