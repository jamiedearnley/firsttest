﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="Scorecard.About" %>
<%@ Register TagPrefix="Nav" TagName="Navigation" Src="~/Controls/Navigation.ascx" %>
<%@ Register TagPrefix="Ftr" TagName="Footer" Src="~/Controls/Footer.ascx" %>
<!DOCTYPE html>
<html lang="en">
 <head>
  <meta charset="utf-8" />
  <meta name="description" content="Shepley cricket club" />
  <meta name="author" content="Jamie Dearnley" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0" />
  <link rel="stylesheet" media="screen, projection" href="/styles/screen.css" />
  <title>Shepley Cricket Club - About</title>
  
  <!-- enable HTML5 elements in IE7+8 -->
  <!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <script type="text/javascript" src="//use.typekit.net/mdu4awa.js"></script>
  <script type="text/javascript">      try { Typekit.load(); } catch (e) { }</script>
  <script type="text/javascript" src="/scripts/jquery-1.8.3.min.js"></script>
 </head>
 <body>
  <div id="container">
   <form runat="server">
   
   <Nav:Navigation runat="server" ID="pageNav" />   
   <article id="about" class="first large">
	<div class="split ls">
	 <h2>ABOUT SHEPLEY CRICKET CLUB</h2>
	 <p>Shepley Cricket Club is a community-based organisation situated in a rural village seven miles south-east of Huddersfield in West Yorkshire.</p>
	 <p>The Club was founded in 1871 and moved to its present Marsh Lane ground in 1888. It has competed in the Huddersfield Cricket League since 1932, and it also has a thriving junior section that has teams from U9s to U17s in the Huddersfield Junior Cricket League.</p>
	 <p>Over the past ten years the Club has moved forward on and off the field. It is proud of its place in village life as hosts of the wonderful Shepley Spring Festival (a three day music festival every May) and of Shepley Village Bonfire (the biggest such event for miles around).</p>
	 <p>The Club welcomes members and guests from all age groups and all backgrounds.</p>
	</div>
	<div class="split rs">
	 <h2>MEMBERSHIP FEES 2013</h2>
	 <p>(In two instalments)</p>
	 <h5>Playing Members: &pound;100.00</h5>
	 <h5>Non-Playing Members: &pound;30.00</h5>
	 <h5>Senior Citizens: &pound;15.00</h5>
	 <h5>Junior Playing Members: &pound;45.00</h5>
	 <p>(Please note that ONLY juniors who are paid up members of the Club as players or in family membership will be permitted to use the Club's facilities, including the nets)</p>
	 <h5>Family Membership: &pound;45.00</h5>
	 <p>(Couple &amp; children under 17, excluding registered Junior players, who must be Junior Members in their own right)</p>
	</div>
	<hr />
   </article>
   
   
   <article id="officals" class="large">
	<div class="split ls">
	 <h2>CLUB OFFICIALS 2013</h2>
	 <p>
	  <table>
	   <tr>
	    <td>President</td>
	    <td>Richard Haigh</td>
	    <td>07811 944461</td>
	   </tr>
	   <tr>
	    <td>Chairman</td>
	    <td>David Hall</td>
	    <td>07803 712323</td>
	   </tr>
	   <tr>
	    <td>Vice-Chairman</td>
	    <td>Sue Glover</td>
	    <td>07825 728611</td>
	   </tr>
	   <tr>
	    <td>Secretary</td>
	    <td>Brian Wells</td>
	    <td>07974 877704</td>
	   </tr>
	   <tr>
	    <td>Treasurer</td>
	    <td>Brian McCrea</td>
	    <td>07784 658611</td>
	   </tr>
	   <tr>
	    <td>Child Welfare</td>
	    <td>Lynda Netherwood</td>
	    <td>07810 518368</td>
	   </tr>
	   <tr>
	    <td>League Rep</td>
	    <td>David Hall</td>
	    <td>07803 712323</td>
	   </tr>
	   <tr>
	    <td>Umpires Rep</td>
	    <td>Garry Booth</td>
	    <td>01484 606061</td>
	   </tr>
	  </table>
	 </p>

	 <h2>MANAGEMENT</h2>
	 <p>
	  <table>
	   <tr>
	    <td>Senior Cricket</td>
	    <td>David Hall</td>
	   </tr>
	   <tr>
	    <td>Junior Cricket</td>
	    <td>Brian Wells</td>
	   </tr>
	   <tr>
	    <td>Ground &amp; Facilities</td>
	    <td>&nbsp;</td>
	   </tr>
	   <tr>
	    <td>Development &amp; Fund Raising</td>
	    <td>John Rae</td>
	   </tr>
	   <tr>
	    <td>&nbsp;</td>
	    <td>Richard Haigh</td>
	   </tr>
	   <tr>
	    <td>Redevelopment</td>
	    <td>Richard Haigh</td>
	   </tr>
	   <tr>
	    <td>Website Administrator</td>
	    <td><a href="mailto:jamie.dearnley@shepleycc.com">Jamie Dearnley</a></td>
	   </tr>
	  </table>
	 </p>
	 
	 <h2>PLAYING</h2>
	 <p>
	  <table>
	   <tr>
	    <td>1st XI Captain</td>
	    <td>Danny Glover</td>
	   </tr>
	   <tr>
	    <td>2nd XI Captain</td>
	    <td>Gary Bradley</td>
	   </tr>
	  </table>
	 </p>

	 <h2>GROUNDSMAN</h2>
	 <p>
	  <table>
	   <tr>
	    <td>John Smith</td>
	    <td>Colin Tinker</td>
	    <td>Brian Wells</td>
	   </tr>
	  </table>
	 </p>
	</div>
	
	<div class="split rs">
	 <h2>COMMITTEE</h2>
	 <p>
	  <table>
	   <tr>
	    <td>Garry Booth</td>
	    <td>Keith Parry</td>
	   </tr>
	   <tr>
	    <td>Jon McKay</td>
	    <td>Beth Rae</td>
	   </tr>
	   <tr>
	    <td>Danny Glover</td>
	    <td>John Rae</td>
	   </tr>
	   <tr>
	    <td>Ian Glover</td>
	    <td>Jamie Dearnley</td>
	   </tr>
	   <tr>
	    <td>Sue Glover</td>
	    <td>Brian Wells</td>
	   </tr>
	   <tr>
	    <td>Brian McCrea</td>
	    <td>Peter Flooks</td>
	   </tr>
	  </table>
	 </p>
	 
	 <h2>QUALIFIED COACHES</h2>
	 <p>
	  <table>
	   <tr>
	    <td>Jamie Dearnley</td>
	    <td>Danny Glover</td>
	   </tr>
	   <tr>
	    <td>Huw Ellis</td>
	    <td>Craig Glover</td>
	   </tr>
	   <tr>
	    <td>John Rae</td>
	    <td>Brian Wells</td>
	   </tr>
	  </table>
	 </p>
	 
	 <h2>DOCUMENTS</h2>
	 <p>Click below to open pdf versions of the clubs consitutions and management structure.</p>
	 <p><a href="/documents/Constitution.pdf" target="_blank">Constitution</a></p>
	 <p><a href="/documents/SCCCodeofConduct.pdf" target="_blank">Code of Conduct</a></p>
	 <p><a href="/documents/JuniorConstitution.pdf" target="_blank">Junior Constitution</a></p>
	 <p><a href="/documents/SCCJuniorCodeofConduct.pdf" target="_blank">Junior Code of Conduct</a></p>
	 <p></p>
	</div>
	<hr />
   </article>

   
   <article id="historyexcerpt" class="large">
	<h1>THE FIRST 100 YEARS</h1>
	<div class="inlinepost">
	 <p>Shepley Cricket Club has a long and proud history dating back to 1871; this makes the club five years older than Test cricket and older than eight of the first-class counties.  This history was brought into focus in 1971 when the club celebrated its centenary.</p>
	 <p>As part of the celebrations, a handbook was produced including a potted history of the first one hundred years by the then President of the club, GA Keen Esq. To read it <a href="/History/" title="Click to read about the history of Shepley CC">click here</a></p>
	</div>
	<hr />
   </article>
   
   
   
   <article id="rollofhonor" class="large">
    <div class="split ls">
	 <h2>INDIVIDUAL AWARDS</h2>
	 <h5>Lady Sykes Candlesticks</h5>
	 <p><em>Presented by the League to an individual player who has given outstanding service to their club and the game in general</em></p>
	 <p>
	  <ul>
	   <li>B Kettlewell 1990</li>
	   <li>D Barber 1998</li>
	   <li>G Gill 2001</li>
	  </ul>
	 </p>
	 
	 <h5>F R Stallard Cup</h5>
	 <p><em>Presented by the League to an individual who, in any capacity, has given outstanding service to their club, the League and the game in general.</em></p>
	 <p>M Hine 2003</p>
	 
	 <h5>Jack Gledhill Memorial Prize</h5>
	 <p><em>Presented to the best all-rounder of the year</em></p>	 
	 <p>
	  <ul>
	   <li>P Heaton 1998</li>
	   <li>SP Singh 2010</li>
	  </ul>
	 </p>
	 
	 <h5>Reg Haigh Trophy</h5>
	 <p><em>Presented to the most promising young cricketer in the Huddersfield Cricket League</em></p>
	 <p>
	  <ul>
	   <li>H Palmer 1979</li>
	   <li>D Glover 2005</li>
	   <li>T Denton 2007</li>
	  </ul>
	 </p>
	 
	 <h5>League Bowling Prize</h5>
	 <p>
	  <ul>
	   <li>G Blackburn 1952</li>
	   <li>G Schofield 1966</li>
	   <li>P Heaton 2004</li>
	  </ul>
	 </p>
	 
	 <h5>League Catching Prize</h5>
	 <p>
	  <ul>
	   <li>C Sutcliffe 1952</li>
	   <li>J T Holmes 1960</li>
	   <li>D Woodward 1963</li>
	   <li>D Midgeley 1966</li>
	   <li>P Walker 1986</li>
	   <li>M Gill 1988</li>
	   <li>T Rees 2009 &amp; 2010</li>
	  </ul>
	 </p>
	</div>
	
	<div class="split rs"> 
	 <h5>League Wicketkeeping Prize</h5>
	 <p>
	  <ul>
	   <li>J T Holmes 1960</li>
	  </ul>
	 </p>
	 
	 <h5>Section B League Batting Prize</h5>
	 <p>
	  <ul>
	   <li>P Heaton 1998</li>
	  </ul>
	 </p>
	 
	 <h5>Section B League Bowling Prize</h5>
	 <p>
	  <ul>
	   <li>I Glover 1998</li>
	  </ul>
	 </p>
	 
	 <h5>Johnny Hunter Rose Bowl</h5>
	 <p><em>The match-winning performance of the Season</em></p>	 
	 <p>
	  <ul>
	   <li>D Glover 2009</li>
	  </ul>
	 </p>
	 
	 <h2>TEAM AWARDS</h2>
	 <p>
	  <ul>
	   <li>Winners Sykes Cup 2011</li>
	   <li>Winners Heavy Woollen Cup 2011</li>
	   <li>Champions Huddersfield Cricket League 1982 &amp; 1984</li>
	   <li>Champions Huddersfield Central League 1925</li>
	   <li>Champions Dearne Valley League 1912</li>
	   <li>Champions Huddersfield Cricket League Section B 1966 &amp; 1998</li>
	   <li>Winners Paddock Shield 1973 &amp; 2008</li>
	   <li>Winners Hopkinson Trophy 1948 &amp; 2009</li>
	   <li>Winners Examiner Trophy 1983 &amp; 1989</li>
	   <li>Winners David Boulton Memorial Trophy 2009</li>
	  </ul>
	 </p>
	 
	 <h2>CLUB AWARDS</h2>
	 <h5>F E Greenwood Ground Trophy</h5>
	 <p><em>Presented by the League to a club whose ground is of a high standard taking into account efforts to improve the playing area and amenities</em></p>
	 <p>
	  <ul>
	   <li>Winners 1962, 1973, 1992, 1993, 2002, 2005 &amp; 2008</li>
	  </ul>
	 </p>
	</div>
	<hr />
   </article>
   
   
   </form>
   <div class="clearfooter"></div>
   </div>

<Ftr:Footer runat="server" ID="pageFooter" />
 </body>
</html>
