﻿<%@ Page Title="Home Page" Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Scorecard._Default" %>
<%@ Register TagPrefix="Nav" TagName="Navigation" Src="~/Controls/Navigation.ascx" %>
<%@ Register TagPrefix="Ftr" TagName="Footer" Src="~/Controls/Footer.ascx" %>
<!DOCTYPE html>
<html lang="en">
 <head>
  <meta charset="utf-8" />
  <meta name="description" content="Shepley cricket club" />
  <meta name="author" content="Jamie Dearnley" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0" />
  <link rel="stylesheet" media="screen, projection" href="/styles/screen.css" />
  <title>Shepley Cricket Club</title>
  
  <!-- enable HTML5 elements in IE7+8 -->
  <!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <script type="text/javascript" src="//use.typekit.net/mdu4awa.js"></script>
  <script type="text/javascript" src="/scripts/jquery-1.8.3.min.js"></script>
  <script type="text/javascript" src="/scripts/jquery.simpleSlide_min.js"></script>
  <script type="text/javascript">      try { Typekit.load(); } catch (e) { }</script>
  <script type="text/javascript">
      $(document).ready(function () {
          simpleSlide(
	    {
	        'swipe': 'true',
	        'set_speed': 300
	    });
      });

      $('.slideshow').live('mouseover mouseout',
		function (event) {
		    if (event.type == 'mouseover') {
		        $(this).children('.left-button, .right-button').stop(true, true).fadeIn();
		    }
		    else {
		        $(this).children('.left-button, .right-button').stop(true, true).fadeOut();
		    }
		}
	);
  </script>
 </head>
 <body>
  <div id="container">
   <form runat="server">
   
   <Nav:Navigation runat="server" ID="pageNav" />
   
   <article id="festival" class="first">
    <time datetime="2013-05-10T08:30">May <span>10</span></time>
    <h1>SHEPLEY SPRING FESTIVAL 2013</h1>
    <div class="inlinepost">
     <p>The Shepley Spring Festival is upon us again! The festival sub-committee has been hard at work organising this years event and we need as many volunteers as possible to help run both the juniors' stall on Saturday and Sunday and the bar from Thursday evening onwards. Any first/second team members or juniors parents who are available please contact either <a href="mailto:smglover3121@talktalk.net">Sue</a> or David (07803712303)</p>
	</div>
	<hr />
   </article>
   
   <article id="postfestivalbandnight">
    <time datetime="2013-05-10T08:51">May <span>10</span></time>
	<h1>CELEBRATE THE 60 YEARS OF QUEEN ELIZABETH'S REIGN</h1>
	<div class="fullpost">
	 <p>We will be celebrating the queens 60th Jubilee by hosting a village event at the club on 25th May from 7pm -12pm. The band Explosion will be playing and there will be a disco and barbecue. £5 per family or £2 per adult.</p>
	</div>
	<hr />
   </article>

   <article id="premhandicap1">
    <time datetime="2013-05-09T08:18">May <span>09</span></time>
    <h1>PREMIERSHIP FOOTBALL HANDICAP AS AT 09/05/2013</h1>
    <div class="inlinepost">
     <p>With 2 games to be played by all leading contenders, the total points, including handicap, for the leaders are as below</p>
     <table>
      <tr>
       <td>=1 West Brom</td>
       <td>89 Paul Kaye</td>
      </tr>
      <tr>
       <td>=1 Swansea</td>
       <td>89 David Hall</td>
      </tr>
      <tr>
       <td>3 Man Utd</td>
       <td>88 Darryl Brook</td>
      </tr>
      <tr>
       <td>4 Everton</td>
       <td>87 Colin Tinker</td>
      </tr>
      <tr>
       <td>5 West Ham</td>
       <td>86 Tom Wordsworth</td>
      </tr>
     </table>
	</div>
	<hr />
   </article>
   
   <article id="u13team6thmay">
    <time datetime="2013-05-07T09:00">May <span>07</span></time>
	<h1>U13 Team for this week</h1>
	<div class="fullpost">
	 <p>
	     Nick lane - captain<br />
         James Netherwood<br />
         Sam Sykes<br />
         Jake Lynn<br />
         William Rae<br />
         George Gumpert<br />
         Leo Berry - wicket keeper ( till Lewis back )<br />
         John Lockwood<br />
         James Coneron<br />
         Toby Holmes<br />
         Luke freeman<br />
         12th man - Tom O'Hara
     </p>
	</div>
	<hr />
   </article>
   
   <article id="u11team6thmay">
    <time datetime="2013-05-07T09:01">May <span>07</span></time>
	<h1>U11 Team for this week</h1>
	<div class="fullpost">
	 <p>
	     Will Rae - captain<br />
         Tommy Gorman<br />
         Jack Longden<br />
         Charlie Smith<br />
         Luke Valentine<br />
         Jordan Williamson<br />
         Oliver Duffell<br />
         Ben Morris<br />
         Owen Smith<br />
         Dan Lane<br />
         12th man - Benjamin Robinson
     </p>
	</div>
	<hr />
   </article>
   
   <article id="aprilreview">
    <time datetime="2013-05-02T08:44">May <span>02</span></time>
	<h1 class="fullwidth">REVIEW FOR APRIL - 1ST and 2ND TEAMS</h1>
	<div class="inlinepostfull">
	 <p>The season started on time on the 20th, despite the mountain of snow that fell on Shepley &amp; surrounding areas just a few weeks before. The first X1 had a convincing 6 pts victory against Hall Bower at Marsh Lane, the first 7 of the Bower wickets all falling to opening bowler Craig Glover, who returned the excellent figures of 7-28 in 15 splendid overs. Our new recruits all figured prominently in their first games for the Club, Josh Clarkson (Tiny) with 4 victims behind the stumps, South African Sean Savage with 2-1 & Danny Wood with 1-0 whilst opener Dan Wood led the home team to an 8 wicket victory with 27 not out. Our 2nd X1 didn’t fair quite as well against the same opponents, Shepley being dismissed for a disappointing 141 after an excellent opening century partnership between new captain Gary Bradley, 65 &amp; the ever dependable Stuart Greaves, 39. In reply, Bower knocked off the required runs for the loss of 6 wickets, Chris Cox finishing with 3-31 &amp; Stuart Greaves with 2-20.<br/></p>
	 <p>The following day in the Crowther Cup, Bradford League team Baildon visited Marsh Lane & posted a total of 198 in their 50 overs, Young Tom James &amp; the returning Ali Reza grabbing a couple of wickets each. Unfortunately, the visitors proved far too strong on the day, bowling out Shepley for a disappointing 59 for a heavy defeat. A much closer game took place at Baildon in the prestigious Heavy Woollen Cup. Baildon posted a competitive total of 228 all out in their 50 overs, Danny Glover &amp; Liam Wiles both finishing with 3 wickets & Danny Wood with 2. The Shepley reply was anchored by the impressive Dan Wood who fell for 76 at a crucial stage of the game. Despite good contributions from Liam Wiles 33 &amp; Oliver Cox 21, Shepley were dismissed to lose the closely fought tie by a mere 6 runs. Even the Baildon supporters thought we had not enjoyed all the best decisions including a caught behind for Sean Savage when the ball clearly appeared to hit him on his shoulder.<br /></p>
	 <p>On the 27th, the 2nd XI entertained Delph & Dobcross who scored 134-2 in a rain affected game of 25 overs a side, Ali Reza taking both wickets to fall. The Shepley reply was positive &amp; a 5 pt victory was secured by 6 wickets with valuable contributions from Rob Denton 47 & an undefeated knock of 46 from Chris Cox. The 1st X1 travelled to Barkisland & in a 40 over game, posted an excellent total of 231-6, the Wood brothers Dan & Greg scoring 47 & 56 respectively, being well supported by Josh Clarkson 40 &amp; Liam Wiles 32. A 5 pts victory was achieved when Barkisland replied with a total of 188-5, captain Danny Glover finishing with 3-52.<br /></p>
     <p>The following day, the first team were again in action at home to near neighbours Shelley. Having won the toss & inserted the visitors, Shelley were all out for 40 Craig & Danny Glover finishing with 4-10 &amp; 3-20 respectively. The dependable Dan Woods &amp; Liam Wiles guided Shepley home to a victory by 8 wickets.</p>
	</div>
	<hr />
   </article>

   <article id="premhandicap2">
    <time datetime="2013-04-16T08:42">Apr <span>16</span></time>
    <h1>PREMIERSHIP FOOTBALL HANDICAP AS AT 14/04/2013</h1>
    <div class="inlinepost">
     <p>Points scored after 32 games, incorporating handicap points</p>
     <table>
      <tr>
       <td>1 West Brom</td>
       <td>85 Paul Kaye</td>
      </tr>
      <tr>
       <td>2 Swansea</td>
       <td>84 David Hall</td>
      </tr>
      <tr>
       <td>3 Southampton</td>
       <td>84 David Broadbent</td>
      </tr>
      <tr>
       <td>4 Man Utd</td>
       <td>83 Darryl Brook</td>
      </tr>
      <tr>
       <td>5 Everton</td>
       <td>82 Colin Tinker</td>
      </tr>
      <tr>
       <td>6 West Ham</td>
       <td>81 Tom Wordsworth</td>
      </tr>
      <tr>
       <td>7 Norwich</td>
       <td>80 Steve Fitton</td>
      </tr>
     </table>
	</div>
	<hr />
   </article>
   
   <article id="seasonpreview">
    <time datetime="2012-03-21T12:01">Mar <span>21</span></time>
	<h1 class="fullwidth">LOOKING AHEAD TO THE 2013 SEASON</h1>
	<div class="inlinepostfull">
	 <p>Before looking forward, a quick farewell to four players from the senior squad who all paid a big part in our double cup winning season of 2011. After many years at the Club, leading batsman Tim Rees has now returned to play cricket nearer to home in his native Lancashire. Tim first appeared on the Shepley scene as a teenager, introduced to the Club by his fellow Lancastrian Phil Heaton, going on to be one of the leading batsman in the Huddersfield League &amp; a superb fielder at first slip from where he won the League’s fielding prize on two consecutive occasions. We also say farewell to our opening batsman &amp; off spinner, SP Singh. SP has joined neighbours Holmfirth. SP became one of the leading all rounders in the League &amp; indeed, he won the League prize for that position on one occasion. The Shepley first team can no longer call on the services of mid order batsman Ben Wells, who has returned to Southowram where he began his cricket as a junior &amp; also our excellent wicket keeper Tom &quot;Munch&quot; Sykes who excelled behind the stumps in his 2 seasons with the Club.</p>
	 <p>Shepley wish all four players all the best for the coming season, but in the case of SP, not when he is playing for Holmfirth in direct opposition to the men from Marsh Lane.</p>
	 <p>Back to the coming season. We are delighted to welcome a number of new players to the ranks &amp; first &amp; second team captains Danny Glover &amp; Gary Bradley will be able to call upon the services of these new additions. In no particular order, a big welcome to opening batsman Dan Wood who joins us from the all conquering Wrenthorpe team. Dan will be reunited with his brother Greg in the Shepley team &amp; when batting with fellow opener Tom Denton, we are likely to have the tallest two openers in Section A. At the risk of causing confusion, we have also been successful in obtaining the services of Danny Wood. Teenager Danny joins us from neighbours Clayton West for whom he enjoyed a very successful wicket–taking season with his slow left arm deliveries. Also joining us from the Clayton West club are the father &amp; son duo of Chris &amp; Tom James. Young Tom has a bright future in the game &amp; Dad Chris will bring some experience to Gary’s 2nd XI side. We are delighted to welcome accomplished keeper Josh (Tiny) Clarkson to our ranks. Tiny joins us from neighbours Skelmanthorpe &amp; like Munch before him, will be a great success behind the stumps. A young man whose formative years in cricket were spent at Shepley, Josh Ardron, is returning to the club after a spell playing at Kirkburton. We are delighted to see Josh back in the Shepley ranks. The first team squad will also see much more of one of our twins, Oliver Cox, this season. Oliver broke into the first team towards the end of last season, with a fair measure of success &amp; with Gary Bradley standing down from 1st XI duties to captain the 2nd XI, Oliver will take up one of the mid order batting positions.</p>
	 <p>Finally, we are pleased to announce the signing of a young man from Cape Town, Sean Savage. Sean is a capable early/mid order batsman &amp; leg spin bowler who has had previous experience playing in the UK in the South (down in Somerset last season). 24yo Sean holds a UK passport &amp; does have a connection with our part of the world in the sense that his Mum spent some of her teenage years living in the Wakefield area with her family. We feel sure that Sean will enjoy his season living &amp; playing cricket at Shepley.</p>
	</div>
	<hr />
   </article>

   <article id="cricketforce">
    <time datetime="2013-03-04T08:47">Mar <span>12</span></time>
	<h1>NATWEST CRICKETFORCE 2013</h1>
	<div class="inlinepost">
	 <p>The club will be participating in NatWest cricketforce day on Sunday 7th April from 10am. It's a fantastic opportunity to help prepare the ground for the coming season so if you can spare an hour to help painting and tidying it would be greatly appreciated. Bacon butties will be available!</p>
	</div>
	<hr />
   </article>
   
   <article id="ColtsSigningOn">
    <time datetime="2013-03-04T08:22">Mar <span>04</span></time>
	<h1>COLTS SIGNING ON NIGHT</h1>
	<div class="fullpost">
	 <p>We have now managed to re-arrange the signing on night for Friday 8th March at Shepley Library from about 5.30. Please come along if you can.<br/></p>
     <p><a href="Documents/SCCJuniorForm2013.pdf" title="Junior Membership Form">Click here</a> for the Junior Membership Form which can be printed off, filled in and signed ready for the 8th. Alternatively there will be blank forms available on the night.</p>
	</div>
	<hr />
   </article>

   <article id="HWCupDraw">
    <time datetime="2013-01-30T18:31">Jan <span>30</span></time>
	<h1>HEAVY WOOLLEN/CROWTHER CUP DRAW</h1>
	<div class="fullpost">
	 <p>The first round draw has been made and The first team have been drawn away at Bradford League side Baildon. The second team will play the same opponents at home. Both games to be played on Sunday 21st April</p>
	</div>
	<hr />
   </article>
   
   </form>
   <div class="clearfooter"></div>
  </div>

<Ftr:Footer runat="server" ID="pageFooter" />
 </body>
</html>