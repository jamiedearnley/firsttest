﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportFixtures.aspx.cs" Inherits="Scorecard.ImportFixtures" %>
<%@ Register TagPrefix="Nav" TagName="Navigation" Src="~/Controls/Navigation.ascx" %>
<%@ Register TagPrefix="Ftr" TagName="Footer" Src="~/Controls/Footer.ascx" %>
<%@ Register TagPrefix="Auth" TagName="Authentication" Src="~/Controls/Authentication.ascx" %>
<!DOCTYPE html>
<html lang="en">
 <head>
  <meta charset="utf-8" />
  <meta name="description" content="Shepley cricket club" />
  <meta name="author" content="Jamie Dearnley" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0" />
  <link rel="stylesheet" media="screen, projection" href="/styles/screen.css" />
  <title>Shepley Cricket Club - IMport Fixtures</title>
  
  <!-- enable HTML5 elements in IE7+8 -->
  <!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <script type="text/javascript" src="//use.typekit.net/mdu4awa.js"></script>
  <script type="text/javascript">      try { Typekit.load(); } catch (e) { }</script>
  <script type="text/javascript" src="/scripts/jquery-1.8.3.min.js"></script>
 </head>
 <body>
  <div id="container">
   <form id="form1" runat="server">
<Nav:Navigation runat="server" ID="pageNav" />
   <br />
    <div id="importfixtures">
        <Auth:Authentication runat="server" Permissions="Import" ID="auth1" />
        
        <h2>Import Fixtures</h2>
        <fieldset>
         <ol>
          <li>
              <asp:Label ID="Label1" runat="server" Text="First select a team"></asp:Label>
              <asp:DropDownList ID="dlTeams" runat="server" DataSourceID="sqlTeams" DataTextField="Name" DataValueField="ID" ></asp:DropDownList>
              <asp:SqlDataSource ID="sqlTeams" runat="server" ConnectionString="<%$ ConnectionStrings:Scorecard %>" SelectCommand="GetTeams" SelectCommandType="StoredProcedure">
               <SelectParameters>
                <asp:Parameter DefaultValue="<%$ AppSettings:Club %>" Name="Club" Type="String" />
               </SelectParameters>
              </asp:SqlDataSource>
          </li>
          <li><label for="FileUpload1">Then choose an Excel spreadsheet to import</label><asp:FileUpload ID="FileUpload1" runat="server" /></li>
          <li><label for="btnImport">After entering your credentials in the box to the right, click </label><asp:Button ID="btnImport" runat="server" Text="Import" onclick="btnImport_Click" /></li>
          <li><asp:Literal ID="ltrDebug" runat="server"></asp:Literal></li>
         </ol>
        </fieldset>
        <br />
        <br />
        <img src="/Images/importfixtures.jpg" title="Example of the Spreadsheet format" />
    </div>
    </form>
   
   <div class="clearfooter"></div>
  </div>
  
<Ftr:Footer runat="server" ID="pageFooter" />
 </body>
</html>