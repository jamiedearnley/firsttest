﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="Scorecard.Register" %>
<%@ Register TagPrefix="Nav" TagName="Navigation" Src="~/Controls/Navigation.ascx" %>
<%@ Register TagPrefix="Ftr" TagName="Footer" Src="~/Controls/Footer.ascx" %>
<!DOCTYPE html>
<html lang="en">
 <head>
  <meta charset="utf-8" />
  <meta name="description" content="Shepley cricket club" />
  <meta name="author" content="Jamie Dearnley" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0" />
  <link rel="stylesheet" media="screen, projection" href="/styles/screen.css" />
  <title>Shepley Cricket Club - Import Results</title>
  
  <!-- enable HTML5 elements in IE7+8 -->
  <!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <script type="text/javascript" src="//use.typekit.net/mdu4awa.js"></script>
  <script type="text/javascript">      try { Typekit.load(); } catch (e) { }</script>
  <script type="text/javascript" src="/scripts/jquery-1.8.3.min.js"></script>
 </head>
 <body>
  <div id="container">
   <form id="form1" runat="server">
<Nav:Navigation runat="server" ID="pageNav" />
   <br />
    <div id="register">
     <div class="authcontrol">
      <fieldset class="authentication">
       <legend class="tk-ff-enzo-web">Details</legend>
         <ol>
          <li><label for="emailaddress" class="tk-ff-enzo-web">Email: </label><asp:TextBox ID="emailaddress" 
                    runat="server" TextMode="Email"></asp:TextBox></li>
          <li>&nbsp;<asp:RequiredFieldValidator ID="emailvalidator" runat="server" 
                    ErrorMessage="You must enter an email address" ControlToValidate="emailaddress" 
                    CssClass="autherror tk-ff-enzo-web" Display="Dynamic"></asp:RequiredFieldValidator> 
          <asp:RegularExpressionValidator ID="emailstructurevalidator" runat="server" 
                    ErrorMessage="You must enter a valid email address" 
                    ControlToValidate="emailaddress" CssClass="autherror tk-ff-enzo-web"
                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                    Display="Dynamic"></asp:RegularExpressionValidator></li>
          <li><label for="password" class="tk-ff-enzo-web">Password: </label><asp:TextBox ID="password" runat="server" TextMode="Password"></asp:TextBox></li>
          <li><label for="reenterpassword" class="tk-ff-enzo-web">Re-enter Password: </label><asp:TextBox ID="reenterpassword" runat="server" TextMode="Password"></asp:TextBox></li>
          <li>&nbsp;<asp:RequiredFieldValidator ID="passwordvalidator" runat="server" 
                    ErrorMessage="you must enter a password" ControlToValidate="password" 
                    CssClass="autherror tk-ff-enzo-web" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:CompareValidator
                        ID="CompareValidator1" runat="server" 
                  ErrorMessage="Passwords do not match" ControlToCompare="password" 
                  ControlToValidate="reenterpassword" CssClass="tk-ff-enzo-web autherror"></asp:CompareValidator></li>
          <li><asp:Button ID="btnSubmit" runat="server" Text="Register" CssClass="submit" 
                  onclick="btnSubmit_Click" /></li>
          <li><asp:Literal ID="ltrAuthError" runat="server"></asp:Literal></li>
       </ol>
      </fieldset>
     </div>
     
     <h2>Register an Account</h2>
     <p class="tk-ff-enzo-web">To import fixtures and results you must register with an email address and password. A confirmation email will be sent to you. Please click on the link in the email to verify your registration<br /><br /></p>
     <asp:Literal ID="ltrDebug" runat="server"></asp:Literal>
    </div>
   </form>
   
   <div class="clearfooter"></div>
  </div>
  
<Ftr:Footer runat="server" ID="pageFooter" />
 </body>
</html>