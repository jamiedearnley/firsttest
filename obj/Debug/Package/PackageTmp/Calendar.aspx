﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Calendar.aspx.cs" Inherits="Scorecard.Calendar" %>
<%@ Register TagPrefix="Nav" TagName="Navigation" Src="~/Controls/Navigation.ascx" %>
<%@ Register TagPrefix="Ftr" TagName="Footer" Src="~/Controls/Footer.ascx" %>
<!DOCTYPE html>
<html lang="en">
 <head>
  <meta charset="utf-8" />
  <meta name="description" content="Shepley cricket club" />
  <meta name="author" content="Jamie Dearnley" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0" />
  <link rel="stylesheet" media="screen, projection" href="/styles/screen.css" />
  <link rel="stylesheet" media="screen, projection" href="/styles/fullcalendar.css" />
  <title>Shepley Cricket Club - Calendar</title>
  
  <!-- enable HTML5 elements in IE7+8 -->
  <!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <script type="text/javascript" src="//use.typekit.net/mdu4awa.js"></script>
  <script type="text/javascript" src="/scripts/jquery-1.8.3.min.js"></script>
  <script type="text/javascript" src="/scripts/fullcalendar.min.js"></script>
  <script type="text/javascript" src="/scripts/gcal.js"></script>
  <script type="text/javascript">      try { Typekit.load(); } catch (e) { }</script>
  <script type='text/javascript'>
      $(document).ready(function () {
          $('#calendar').fullCalendar({
              events: {
                  url: 'https://www.google.com/calendar/feeds/jeiqmbsq7b5nfo6jc4b6qf9lgk%40group.calendar.google.com/public/basic',
                  className: 'gcal-event'
              }
          });
      });
  </script>
 </head>
 <body>
  <div id="container">
   <form runat="server">
   <Nav:Navigation runat="server" ID="pageNav" />
    <div id="calendarinfo"><p>The club calendar is now hosted on Google Calendar. This makes adding, editing and sharing calendar events much easier. If you would like permissions to edit the calendar please contact the system administrator.<br /><br /></p>
    <p>To link the calendar to your phone or tablet please click this link: <a href="https://www.google.com/calendar/ical/jeiqmbsq7b5nfo6jc4b6qf9lgk%40group.calendar.google.com/public/basic.ics">Public Calendar</a></p></div>
    <div id="calendar"></div>
   </form>
    <div class="clearfooter"></div>

  </div>
  
<Ftr:Footer runat="server" ID="pageFooter" />
 </body>
</html>
