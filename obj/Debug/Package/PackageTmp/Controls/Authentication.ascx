﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Authentication.ascx.cs" Inherits="Scorecard.Controls.Authentication" %>

<asp:HiddenField ID="hiddenFlags" runat="server" />

<div class="authcontrol">
 <fieldset class="authentication">
  <legend class="tk-ff-enzo-web">Authentication</legend>
    <ol>
     <li><label for="emailaddress" class="tk-ff-enzo-web">Email: </label><asp:TextBox ID="emailaddress" 
                runat="server" TextMode="Email"></asp:TextBox></li>
     <li>&nbsp;<asp:RequiredFieldValidator ID="emailvalidator" runat="server" 
                ErrorMessage="You must enter an email address" ControlToValidate="emailaddress" 
                CssClass="autherror tk-ff-enzo-web" Display="Dynamic"></asp:RequiredFieldValidator> 
     <asp:RegularExpressionValidator ID="emailstructurevalidator" runat="server" 
                ErrorMessage="You must enter a valid email address" 
                ControlToValidate="emailaddress" CssClass="autherror tk-ff-enzo-web"
                ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                Display="Dynamic"></asp:RegularExpressionValidator></li>
     <li><label for="password" class="tk-ff-enzo-web">Password: </label><asp:TextBox ID="password" runat="server" TextMode="Password"></asp:TextBox></li>
     <li>&nbsp;<asp:RequiredFieldValidator ID="passwordvalidator" runat="server" 
                ErrorMessage="you must enter a password" ControlToValidate="password" 
                CssClass="autherror tk-ff-enzo-web" Display="Dynamic"></asp:RequiredFieldValidator></li>
     <li><asp:Literal ID="ltrAuthError" runat="server"></asp:Literal></li>
  </ol>
 </fieldset>
</div>