﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Scorecard.ascx.cs" Inherits="Scorecard.Controls.Scorecard" %>
<%@ Register TagPrefix="in" TagName="Innings" Src="~/Controls/Innings.ascx" %>

<asp:Literal runat="server" ID="result"></asp:Literal>
<asp:Literal runat="server" ID="report"></asp:Literal>
<br /><br />

<div id="firstinnings">
     <in:Innings ID="first" runat="server" />
</div>

<br /><br /><br />

<div id="secondinnings">
     <in:Innings ID="second" runat="server" />
</div>