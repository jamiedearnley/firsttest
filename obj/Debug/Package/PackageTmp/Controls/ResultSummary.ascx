﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResultSummary.ascx.cs" Inherits="Scorecard.Controls.ResultSummary" %>
<%@ Register TagPrefix="ru" TagName="ResultURL" Src="~/Controls/ResultURL.ascx" %>

<asp:SqlDataSource ID="sqlFixtures" runat="server" 
           ConnectionString="<%$ ConnectionStrings:Scorecard %>"   
           SelectCommand="ResultDate" 
           SelectCommandType="StoredProcedure"
           OnSelecting="UpdateSqlParametersEventHandler">
           <SelectParameters>
            <asp:Parameter Name="Club" Type="String" />
            <asp:Parameter Name="Date" Type="DateTime" />
           </SelectParameters>
      </asp:SqlDataSource>
      
      <asp:Repeater
         runat="server"
         id="MenuItems"
         DataSourceID="sqlFixtures">
         <ItemTemplate>
             <ru:ResultURL runat="server" Result=<%# DataBinder.Eval(Container.DataItem, "ID") %> />
         </ItemTemplate>
      </asp:Repeater>