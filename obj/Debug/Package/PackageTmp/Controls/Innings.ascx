﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Innings.ascx.cs" Inherits="Scorecard.Controls.Innings" %>
<%@ Register TagPrefix="ib" TagName="InningsBatters" Src="~/Controls/InningsBatters.ascx" %>
<%@ Register TagPrefix="ibl" TagName="InningsBowlers" Src="~/Controls/InningsBowlers.ascx" %>

<div class="innings">
<asp:Literal runat="server" ID="title"></asp:Literal>
<ib:InningsBatters runat="server" ID="batters" />
<br /><br />
<ibl:InningsBowlers runat="server" ID="bowlers" />
</div>
