﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Footer.ascx.cs" ClassName="Footer" Inherits="Scorecard.Footer" %>

<div id="clubinfo">
   <div id="wrapper">
    <div id="contact" class="br">
	 <div id="seniorcric" class="bb section">
	  <h5>SENIOR CRICKET</h5>
	  <p><em>David Hall</em><br />72 The Knowle. Shepley,<br /> HD8 8EA<br />(01484) 607895<br />(07803) 712303<br /><a href="mailto:david.hall@shepleycc.com" class="lnk">david.hall@shepleycc.com</a></p>
	  <p><em>Brian Wells</em><br />123 Marsh Lane, Shepley, <br />HD8 8AS<br />(07974) 877704<br /><br /><a href="mailto:brian.wells@shepleycc.com" class="lnk">brian.wells@shepleycc.com</a></p>
	 </div>
	 <div id="juniorcric" class="bb section">
	  <h5>JUNIOR CRICKET</h5>
	  <p class="small"><em>Beth Rae</em><br />(01484) 604284</p>
	  <p><a href="mailto:beth.rae@shepleycc.com" class="lnk">beth.rae@shepleycc.com</a><br />(07798) 612958</p>
	 </div>
	 <div id="welfare" class="section">
	  <img src="/images/lynda.gif" alt="Lynda Netherwood" title="Shepley Child Welfare Officer: Lynda Netherwood" />
	  <h5>CHILD WELFARE</h5>
	  <p>Lynda Netherwood. Lynda can be contacted at any time.<br /><br />43 Abbey Drive, Shepley, HD8 8DX<br /><a href="mailto:lynda.netherwood@shepleycc.com" class="lnk">lynda.netherwood@shepleycc.com</a> (07810) 518368</p>
	 </div>
	</div>
	<div id="sponsors" class="br section">
	 <h5>SPONSORS</h5>
	 <a href="http://www.huddersfieldcricketleague.co.uk"><img src="/images/banner-drakes.jpg" alt="Drakes Cricket League Logo" title="Click to browse to the Drakes Cricket League website" /></a>
     <a href="http://www.clubmark.org.uk/"><img src="/images/banner-clubmark.jpg" alt="Clubmark Logo" title="Click to browse to the Club Mark website" /></a>
	 <a href="http://www.willowsportswear.co.uk"><img src="/images/banner-willow.jpg" alt="Willow Sportswear Logo" title="Click to browse to the Willow Sportswear website" /></a>
	 <a href="http://www.gtaccountancyservices.co.uk/"><img src="/images/banner-gandt.jpg" alt="G &amp; T Accountancy Logo" title="Click to browse to the G &amp; T Accountancy Services website" /></a>
	 <a href="http://www.ronandevelopments.com"><img src="/images/banner-ronan.jpg" alt="Ronan Developments Logo" title="Click to browse to the Ronan Developments website" /></a>
	 <a href="http://www.shepleyspringfestival.com"><img src="/images/banner-festival.jpg" alt="Shepley Spring Festival Logo" title="Click to browse to the Shepley Spring Festival website" /></a>
	 <a href="http://www.hjcl.org.uk/"><img src="/images/banner-sellers.jpg" alt="Sellers Junior League Logo" title="Click to browse to the Sellers Huddersfield Junior Cricket League website" /></a>
	 <a href="http://www.allroundercricket.com/"><img src="/images/banner-allrounder.jpg" alt="All Rounder Cricket Logo" title="Click to browse to the All Rounder Cricket website" /></a>
	</div>	
	<div id="directions" class="section">
	 <h5>DIRECTIONS</h5>
	 <iframe width="338" height="250" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.uk/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=shepley+cricket+club&amp;aq=&amp;sll=52.8382,-2.327815&amp;sspn=10.655039,24.587402&amp;t=h&amp;ie=UTF8&amp;hq=shepley+cricket+club&amp;ll=53.582684,-1.721528&amp;spn=0.013248,0.032015&amp;output=embed"></iframe><br /><small><a href="https://maps.google.co.uk/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=shepley+cricket+club&amp;aq=&amp;sll=52.8382,-2.327815&amp;sspn=10.655039,24.587402&amp;t=h&amp;ie=UTF8&amp;hq=shepley+cricket+club&amp;ll=53.582684,-1.721528&amp;spn=0.013248,0.032015" class="lnk">View Larger Map</a></small>
	</div>
    <footer role="siteinfo">
     <p>&copy; Shepley Cricket Club 2012 - Website Administration: <a href="mailto:jamie.dearnley@shepleycc.com" class="lnk">jamie.dearnley@shepleycc.com</a></p>
    </footer>
   </div>
  </div>
  <script type="text/javascript">
      var images = ['header01.jpg', 'header02.jpg', 'header03.jpg', 'header04.jpg'];
      $('<img src="/images/' + images[Math.floor(Math.random() * images.length)] + '">').appendTo('#headerbg');
  </script>
  <script type="text/javascript">
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-37609585-1']);
      _gaq.push(['_trackPageview']);

      (function () {
          var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
          ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
          var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
  </script>