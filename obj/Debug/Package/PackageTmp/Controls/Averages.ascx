﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Averages.ascx.cs" Inherits="Scorecard.Controls.Averages" %>

<asp:SqlDataSource ID="sqlBattingAverages" runat="server" 
    ConnectionString="<%$ ConnectionStrings:Scorecard %>"   
    SelectCommand="GetBattingAverages" 
    SelectCommandType="StoredProcedure"
    OnSelecting="UpdateSqlParametersEventHandler">
    <SelectParameters>
        <asp:Parameter Name="Club" Type="String" />
        <asp:Parameter Name="Team" Type="String" />
        <asp:Parameter Name="Year" Type="DateTime" />
    </SelectParameters>
</asp:SqlDataSource>

<asp:SqlDataSource ID="sqlBowlingAverages" runat="server" 
    ConnectionString="<%$ ConnectionStrings:Scorecard %>"   
    SelectCommand="GetBowlingAverages" 
    SelectCommandType="StoredProcedure"
    OnSelecting="UpdateSqlParametersEventHandler">
    <SelectParameters>
        <asp:Parameter Name="Club" Type="String" />
        <asp:Parameter Name="Team" Type="String" />
        <asp:Parameter Name="Year" Type="DateTime" />
    </SelectParameters>
</asp:SqlDataSource>

<div id="averagesctrl">
 <asp:Literal runat="server" ID="title"></asp:Literal>
 <br />
 <asp:Repeater
         runat="server"
         id="BattersGrid"
         DataSourceID="sqlBattingAverages">
        <HeaderTemplate>
 <h2>BATTERS</h2>
 <table class="averages">
  <tr class="blankrow">
   <th align="left">&nbsp;</th>
   <th align="center" class="tk-ff-enzo-web">INNINGS</th>
   <th align="center" class="tk-ff-enzo-web">NOT OUTS</th>
   <th align="center" class="tk-ff-enzo-web">HIGH SCORE</th>
   <th align="center" class="tk-ff-enzo-web">RUNS</th>
   <th align="center" class="tk-ff-enzo-web">AVERAGE</th>
   <th align="center" class="tk-ff-enzo-web">CATCHES</th>
   <th align="center" class="tk-ff-enzo-web">STUMPINGS</th>
  </tr>
        </HeaderTemplate>
        <ItemTemplate>
  <tr>
   <td align="left" class="tk-ff-enzo-web playername"><%# Eval("Name")%></td>
   <td align="center" class="tk-ff-enzo-web"><%# Eval("Innings")%></td>
   <td align="center" class="tk-ff-enzo-web"><%# Eval("NotOuts")%></td>
   <td align="center" class="tk-ff-enzo-web"><%# Eval("HighScore")%></td>
   <td align="center" class="tk-ff-enzo-web"><%# Eval("Runs")%></td>
   <td align="center" class="tk-ff-enzo-web"><%# Eval("Average")%></td>
   <td align="center" class="tk-ff-enzo-web"><%# Eval("Catches")%></td>
   <td align="center" class="tk-ff-enzo-web"><%# Eval("Stumpings")%></td>
  </tr>
        </ItemTemplate>
        <FooterTemplate>
 </table>
        </FooterTemplate>
 </asp:Repeater>
 
 <br />
 <br />

 <asp:Repeater
         runat="server"
         id="BowlersGrid"
         DataSourceID="sqlBowlingAverages">
        <HeaderTemplate>
 <h2>BOWLERS</h2>
 <table class="averages">
  <tr class="blankrow">
   <th align="left">&nbsp;</th>
   <th align="center" class="tk-ff-enzo-web">OVERS</th>
   <th align="center" class="tk-ff-enzo-web">MAIDENS</th>
   <th align="center" class="tk-ff-enzo-web">RUNS</th>
   <th align="center" class="tk-ff-enzo-web">WICKETS</th>
   <th align="center" class="tk-ff-enzo-web">AVERAGE</th>
   <th align="center" class="tk-ff-enzo-web">ECONOMY</th>
   <th align="center" class="tk-ff-enzo-web">STRIKE RATE</th>
  </tr>
        </HeaderTemplate>
        <ItemTemplate>
  <tr>
   <td align="left" class="tk-ff-enzo-web playername"><%# Eval("Name")%></td>
   <td align="center" class="tk-ff-enzo-web"><%# Eval("Overs")%></td>
   <td align="center" class="tk-ff-enzo-web"><%# Eval("Maidens")%></td>
   <td align="center" class="tk-ff-enzo-web"><%# Eval("Runs")%></td>
   <td align="center" class="tk-ff-enzo-web"><%# Eval("Wickets")%></td>
   <td align="center" class="tk-ff-enzo-web"><%# Eval("Average")%></td>
   <td align="center" class="tk-ff-enzo-web"><%# Eval("Economy")%></td>
   <td align="center" class="tk-ff-enzo-web"><%# Eval("StrikeRate")%></td>
  </tr>
        </ItemTemplate>
        <FooterTemplate>
 </table>
        </FooterTemplate>
 </asp:Repeater>
</div>