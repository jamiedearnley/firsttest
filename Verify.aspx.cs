﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Scorecard
{
    public partial class Verify : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["token"]))
            {
                if (VerifyAccount(Request.QueryString["token"]))
                {
                    ltrResult.Text = "<p class=\"tk-ff-enzo-web\">Thankyou for verifying your account</p>";
                }
                else
                {
                    Response.Redirect("Default.aspx");
                }
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }

        private bool VerifyAccount(string token)
        {
            var result = false;
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "VerifyAccount";
                cmd.Parameters.AddWithValue("@Token", token);
                var accountid = cmd.Parameters.Add("@ID", SqlDbType.Int);
                accountid.Direction = ParameterDirection.Output;
                conn.Open();
                cmd.ExecuteScalar();

                if ((int)cmd.Parameters["@ID"].Value > -1)
                {
                    result = true;
                }
                
                conn.Close();
            }

            return result;
        }
    }

}