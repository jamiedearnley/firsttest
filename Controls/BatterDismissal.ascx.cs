﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Scorecard.Controls
{
    public partial class BatterDismissal : System.Web.UI.UserControl
    {
        public string Dismissal
        {
            get { return hiddenDismissal.Value; }
            set
            {
                hiddenDismissal.Value = value;
                ControlLoaded();
            }
        }

        private void ControlLoaded()
        {
            if (!string.IsNullOrEmpty(hiddenDismissal.Value))
            {
                dismissal.Text = GeDismissal(Convert.ToInt32(hiddenDismissal.Value));
            }
        }

        private static string GeDismissal(int dismissal)
        {
            string result;
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "FindDismissal";

                cmd.Parameters.AddWithValue("@Dismissal", dismissal);
                var typeparam = cmd.Parameters.Add("@Type", SqlDbType.NVarChar);
                typeparam.Size = 50;
                typeparam.Direction = ParameterDirection.Output;
                var codeparam = cmd.Parameters.Add("@Code", SqlDbType.NVarChar);
                codeparam.Size = 2;
                codeparam.Direction = ParameterDirection.Output;
                var bowlerparam = cmd.Parameters.Add("@Bowler", SqlDbType.NVarChar);
                bowlerparam.Size = 100;
                bowlerparam.Direction = ParameterDirection.Output;
                var fielderparam = cmd.Parameters.Add("@Fielder", SqlDbType.NVarChar);
                fielderparam.Size = 100;
                fielderparam.Direction = ParameterDirection.Output;
                conn.Open();
                cmd.ExecuteScalar();
                var type = (string)cmd.Parameters["@Type"].Value;
                var code = (string)cmd.Parameters["@Code"].Value;
                var bowler = (string)cmd.Parameters["@Bowler"].Value;
                var fielder = (string)cmd.Parameters["@Fielder"].Value;

                switch (code)
                {
                    case "b":
                        {
                            result = "<td>&nbsp;</td><td class=\"tk-ff-enzo-web\">b " + bowler + "</td>";
                            break;
                        }
                    case "l":
                        {
                            result = "<td class=\"tk-ff-enzo-web\">" + type + "</td><td class=\"tk-ff-enzo-web\">b " + bowler + "</td>";
                            break;
                        }
                    case "s":
                        {
                            result = "<td class=\"tk-ff-enzo-web\">" + type + " " + fielder + "</td><td class=\"tk-ff-enzo-web\">b " + bowler + "</td>";
                            break;
                        }
                    case "c":
                        {
                            if (bowler.Equals(fielder))
                            {
                                result = "<td class=\"tk-ff-enzo-web\">Caught and Bowled</td><td class=\"tk-ff-enzo-web\">b " + bowler + "</td>";
                            }
                            else
                            {
                                result = "<td class=\"tk-ff-enzo-web\">" + type + " " + fielder + "</td><td class=\"tk-ff-enzo-web\">b " + bowler + "</td>";
                            }
                            break;
                        }
                    default:
                        {
                            result = "<td colspan=\"2\" align=\"left\" class=\"tk-ff-enzo-web\">" + type + "</td>";
                            break;
                        }
                }
                conn.Close();
            }

            return result;
        }
    }
}