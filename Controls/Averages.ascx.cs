﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Scorecard.Controls
{
    public partial class Averages : System.Web.UI.UserControl
    {
        private DateTime? _year = null;
        public string Team { get; set; }
        public DateTime Year
        {
            get
            {
                try
                {
                    if (_year == null)
                    {
                        _year = DateTime.Now;
                    }
                    return Convert.ToDateTime(_year);
                }
                catch (Exception)
                {
                    return DateTime.Now;
                }
            }
            set { _year = value; }
        }

        protected void UpdateSqlParametersEventHandler(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@Team"].Value = Team;
            e.Command.Parameters["@Year"].Value = Year;
            e.Command.Parameters["@Club"].Value = ConfigurationManager.AppSettings["Club"];

            title.Text = "<h2>" + ConfigurationManager.AppSettings["Club"].ToUpper() + " " + Team + " AVERAGES - " + Year.Year + "</h2>";
        }
    }
}