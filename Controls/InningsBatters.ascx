﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InningsBatters.ascx.cs" Inherits="Scorecard.Controls.InningsBatters" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Register TagPrefix="b" TagName="Dismissal" Src="~/Controls/BatterDismissal.ascx" %>

<asp:HiddenField ID="hiddenScore" runat="server" />
<asp:HiddenField ID="hiddenExtras" runat="server" />

<asp:SqlDataSource ID="sqlInningsBatters" runat="server" 
    ConnectionString="<%$ ConnectionStrings:Scorecard %>"   
    SelectCommand="GetInningsBatters" 
    SelectCommandType="StoredProcedure"
    OnSelecting="UpdateSqlParametersEventHandler">
    <SelectParameters>
        <asp:Parameter Name="Innings" Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>

<div id="batters">
    <asp:Repeater
         runat="server"
         id="BattersGrid"
         DataSourceID="sqlInningsBatters">
        <HeaderTemplate>
            <table class="inningsbatters">
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td class="battername tk-ff-enzo-web"><%# Eval("Batter")%></td>
                <b:Dismissal ID="dismissal" Dismissal='<%# Eval("ID")%>' runat="server" />
                <td class="batterscore tk-ff-enzo-web" align="right"><%# Eval("Score")%></td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
             <tr class="batterfooter">
                 <td colspan="2">&nbsp;</td>
                 <td class="tk-ff-enzo-web"><em>Extras</em></td>
                 <td class="batterscore tk-ff-enzo-web" align="right"><em><%# hiddenExtras.Value.ToString(CultureInfo.InvariantCulture) %></em></td>
             </tr>
             <tr>
                 <td colspan="2">&nbsp;</td>
                 <td class="tk-ff-enzo-web"><em>Total</em></td>
                 <td class="batterscore tk-ff-enzo-web" align="right"><em><%# hiddenScore.Value.ToString(CultureInfo.InvariantCulture) %></em></td>
             </tr>
            </table>
        </FooterTemplate>
     </asp:Repeater>
</div>