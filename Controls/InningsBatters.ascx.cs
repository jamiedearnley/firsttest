﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Scorecard.Controls
{
    public partial class InningsBatters : System.Web.UI.UserControl
    {
        private int? _id;
        public int InningsId
        {
            get
            {
                if (_id == null)
                {
                    _id = -1;
                }
                return Convert.ToInt32(_id);
            }
            set
            {
                _id = value;
            }
        }

        public int Extras
        {
            get { return Convert.ToInt32(hiddenExtras.Value); }
            set
            {
                hiddenExtras.Value = value.ToString(CultureInfo.InvariantCulture);
            }
        }
        public int Score
        {
            get { return Convert.ToInt32(hiddenScore.Value); }
            set
            {
                hiddenScore.Value = value.ToString(CultureInfo.InvariantCulture);
            }
        }

        protected void UpdateSqlParametersEventHandler(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@Innings"].Value = InningsId;
            GetInningsScore(InningsId);
        }

        private void GetInningsScore(int innings)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetInningsScore";

                cmd.Parameters.AddWithValue("@Innings", innings);
                var score = cmd.Parameters.Add("@Score", SqlDbType.Int);
                score.Direction = ParameterDirection.Output;
                var extras = cmd.Parameters.Add("@Extras", SqlDbType.Int);
                extras.Direction = ParameterDirection.Output;
                conn.Open();
                cmd.ExecuteScalar();

                hiddenScore.Value = Convert.ToString(cmd.Parameters["@Score"].Value);
                hiddenExtras.Value = Convert.ToString(cmd.Parameters["@Extras"].Value);
                conn.Close();
            }
        }
    }
}