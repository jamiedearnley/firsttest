﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using Scorecard.Controls;

namespace Scorecard
{
    public partial class Fixtures : System.Web.UI.UserControl
    {
        private DateTime? _year = null;
        public string Team { get; set; }
        public DateTime Year
        {
            get
            {
                try
                {
                    if (_year == null)
                    {
                        _year = DateTime.Now;
                    }
                    return Convert.ToDateTime(_year);
                }
                catch (Exception)
                {
                    return DateTime.Now;
                }
            }
            set { _year = value; }
        }

        protected void UpdateSqlParametersEventHandler(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@Team"].Value = Team;
            e.Command.Parameters["@Year"].Value = Year;
            e.Command.Parameters["@Club"].Value = ConfigurationManager.AppSettings["Club"];

            title.Text = "<h2>" + ConfigurationManager.AppSettings["Club"].ToUpper() + " " + Team + " FIXTURES - " + Year.Year + "</h2>";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var previous = GetFixtureCount(ConfigurationManager.AppSettings["Club"], Team, Year.AddYears(-1));
            var next = GetFixtureCount(ConfigurationManager.AppSettings["Club"], Team, Year.AddYears(+1));

            if (previous > 0)
            {
                var prevyear = (Year.Year - 1);
                prevFixtures.Text = "<a href='/Team/" + Team + "/" + prevyear + "' id='prevFixtures' title='Click to view the " + prevyear + " fixtures'>&larr; " + prevyear + "</a>";
            }

            if (next > 0)
            {
                var nextyear = (Year.Year + 1);
                nextFixtures.Text = "<a href='/Team/" + Team + "/" + nextyear + "' id='nextFixtures' title='Click to view the " + nextyear + " fixtures'>" + nextyear + " &rarr;</a>";
            }
        }

        private int GetFixtureCount(string club, string team, DateTime year)
        {
            int result;
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetFixtureCount";

                cmd.Parameters.AddWithValue("@Club", club);
                cmd.Parameters.AddWithValue("@Team", team);
                cmd.Parameters.AddWithValue("@Year", year);
                var fixtureId = cmd.Parameters.Add("@Total", SqlDbType.Int);
                fixtureId.Direction = ParameterDirection.Output;
                conn.Open();
                cmd.ExecuteScalar();
                result = (int)cmd.Parameters["@Total"].Value;
                conn.Close();
            }

            return result;
        }
    }
}