﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Scorecard
{
    public partial class FixtureSummary : System.Web.UI.UserControl
    {
        private DateTime? _date;
        public DateTime Date
        {
            get
            {
                try
                {
                    if (_date == null)
                    {
                        _date = DateTime.Now;
                    }
                    return Convert.ToDateTime(_date);
                }
                catch (Exception)
                {
                    return DateTime.Now;
                }
            }
            set { _date = value; }
        }

        protected void UpdateSqlParametersEventHandler(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@Date"].Value = Date;
            e.Command.Parameters["@Club"].Value = ConfigurationManager.AppSettings["Club"];
        }
    }
}