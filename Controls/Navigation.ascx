﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Navigation.ascx.cs" ClassName="Navigation" Inherits="Scorecard.Navigation" %>
<%@ Register TagPrefix="Fix" TagName="Fixture" Src="~/Controls/FixtureSummary.ascx" %>
<%@ Register TagPrefix="Res" TagName="Result" Src="~/Controls/ResultSummary.ascx" %>

<header role="masthead" id="masthead">
    <div id="headerbg"></div>
	
	<div id="matchinfo">
	 <div id="results" class="container">
	  <h2>RESULTS</h2>
	  <div>
	   <asp:SqlDataSource ID="sqlResults" runat="server" 
           ConnectionString="<%$ ConnectionStrings:Scorecard %>"   
           SelectCommand="ResultsSummary" 
           SelectCommandType="StoredProcedure"
           OnSelecting="UpdateSqlParametersEventHandler">
           <SelectParameters>
            <asp:Parameter Name="Club" Type="String" />
            <asp:Parameter Name="Date" Type="DateTime" />
           </SelectParameters>
      </asp:SqlDataSource>
      
      <asp:Repeater
         runat="server"
         id="ResultItems"
         DataSourceID="sqlResults">
        <HeaderTemplate>
        </HeaderTemplate>
         <ItemTemplate>
          <div>
           <time datetime="<%# Eval("Date")%>"><%# DataBinder.Eval(Container.DataItem, "Date", "{0:ddd dd MMMM yyyy ' - ' h tt}").ToUpper() %></time>
           <Res:Result runat="server" Date=<%# DataBinder.Eval(Container.DataItem, "Date") %> />
          </div>
         </ItemTemplate>
        <FooterTemplate>
         </FooterTemplate>
      </asp:Repeater>
	  </div>
	 </div>
	 
	 <div id="fixtures" class="container">
	  <h2>FIXTURES</h2>
      <asp:SqlDataSource ID="sqlFixtures" runat="server" 
           ConnectionString="<%$ ConnectionStrings:Scorecard %>"   
           SelectCommand="FixturesSummary" 
           SelectCommandType="StoredProcedure"
           OnSelecting="UpdateSqlParametersEventHandler">
           <SelectParameters>
            <asp:Parameter Name="Club" Type="String" />
            <asp:Parameter Name="Date" Type="DateTime" />
           </SelectParameters>
      </asp:SqlDataSource>
      
      <asp:Repeater
         runat="server"
         id="FixtureItems"
         DataSourceID="sqlFixtures">
        <HeaderTemplate>
        </HeaderTemplate>
         <ItemTemplate>
          <div>
           <time datetime="<%# Eval("Date")%>"><%# DataBinder.Eval(Container.DataItem, "Date", "{0:ddd dd MMMM yyyy ' - ' h tt}").ToUpper()%></time>
           <Fix:Fixture runat="server" Date=<%# DataBinder.Eval(Container.DataItem, "Date") %> />
          </div>
         </ItemTemplate>
        <FooterTemplate>
         </FooterTemplate>
      </asp:Repeater>
	 </div>
	</div>
	
    <nav role="navigation">
     <ul class="mainnav">
      <li><a href="/Sponsorship/">SPONSORSHIP</a></li>
      <li><a href="/About/">ABOUT</a></li>
      <li><a href="/Calendar/">CALENDAR</a></li>
      <li><a href="/JuniorCricket/">JUNIOR CRICKET</a></li>
      <li><a href="/Team/B/">2ND TEAM</a></li>
      <li><a href="/Team/A/">1ST TEAM</a></li>
      <li><a href="/">HOME</a></li>
     </ul>
    </nav>
   </header>