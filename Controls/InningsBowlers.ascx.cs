﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Scorecard.Controls
{
    public partial class InningsBowlers : System.Web.UI.UserControl
    {
        private int? _id;
        public int InningsId
        {
            get
            {
                if (_id == null)
                {
                    _id = -1;
                }
                return Convert.ToInt32(_id);
            }
            set
            {
                _id = value;
            }
        }

        protected void UpdateSqlParametersEventHandler(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@Innings"].Value = InningsId;
        }
    }
}