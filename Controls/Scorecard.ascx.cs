﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Scorecard.Controls
{
    public partial class Scorecard : System.Web.UI.UserControl
    {
        private int? _id;

        public int FixtureId
        {
            get
            {
                if (_id == null)
                {
                    _id = -1;
                }
                return Convert.ToInt32(_id);
            }
            set 
            {
                _id = value;
                LoadScorecard();
            }
        }

        public void LoadScorecard()
        {
            result.Text = FixtureId != -1 ? GetResult(FixtureId) : "<h2>Fixture has not been completed</h2>";
            report.Text = FixtureId != -1 ? GetReport(FixtureId) : "<p>&nbsp;</p>";
            GetInningsIDs(FixtureId);
        }

        private static string GetReport(int fixture)
        {
            string getReport;
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "FixtureReport";

                cmd.Parameters.AddWithValue("@Fixture", fixture);
                var temp = cmd.Parameters.Add("@Result", SqlDbType.NVarChar);
                temp.Size = 4000;
                temp.Direction = ParameterDirection.Output;
                conn.Open();
                cmd.ExecuteScalar();
                getReport = "<p>" + cmd.Parameters["@Result"].Value + "</p>";
                conn.Close();
            }

            return getReport;
        }

        private static string GetResult(int fixture)
        {
            string result;
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "FixtureResult";

                cmd.Parameters.AddWithValue("@Fixture", fixture);
                var temp = cmd.Parameters.Add("@Result", SqlDbType.NVarChar);
                temp.Size = 100;
                temp.Direction = ParameterDirection.Output;
                conn.Open();
                cmd.ExecuteScalar();
                result = "<h2>" + cmd.Parameters["@Result"].Value + "</h2>";
                conn.Close();
            }

            return result;
        }

        private void GetInningsIDs(int fixture)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetInningsIds";

                cmd.Parameters.AddWithValue("@Fixture", fixture);
                var firstinnings = cmd.Parameters.Add("@First", SqlDbType.Int);
                firstinnings.Direction = ParameterDirection.Output;
                var secondinnings = cmd.Parameters.Add("@Second", SqlDbType.Int);
                secondinnings.Direction = ParameterDirection.Output;
                conn.Open();
                cmd.ExecuteScalar();

                first.InningsId = Convert.ToInt32(cmd.Parameters["@First"].Value);
                second.InningsId = Convert.ToInt32(cmd.Parameters["@Second"].Value);
                conn.Close();
            }
        }
    }
}