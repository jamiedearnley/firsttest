﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Scorecard.Controls
{
    public partial class FixtureHighlight : System.Web.UI.UserControl
    {
        public string Fixture
        {
            get { return hiddenFixture.Value; }
            set
            {
                hiddenFixture.Value = value;
                ControlLoaded();
            }
        }

        private void ControlLoaded()
        {
            if (!string.IsNullOrEmpty(hiddenFixture.Value))
            {
                cssclass.Text = "<tr class=\"" + GetResult(Convert.ToInt32(hiddenFixture.Value)) + "\">";
            }
        }

        private static string GetResult(int fixture)
        {
            string result;
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "FixtureResultbyClub";

                cmd.Parameters.AddWithValue("@Fixture", fixture);
                cmd.Parameters.AddWithValue("@Club", ConfigurationManager.AppSettings["Club"]);
                var temp = cmd.Parameters.Add("@Result", SqlDbType.NVarChar);
                temp.Size = 50;
                temp.Direction = ParameterDirection.Output;
                conn.Open();
                cmd.ExecuteScalar();
                result = (string)cmd.Parameters["@Result"].Value;
                conn.Close();
            }

            return result;
        }
    }
}