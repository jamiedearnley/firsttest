﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Scorecard.Controls
{
    public partial class Innings : System.Web.UI.UserControl
    {
        private int? _id;

        public int InningsId
        {
            get
            {
                if (_id == null)
                {
                    _id = -1;
                }
                return Convert.ToInt32(_id);
            }
            set
            {
                _id = value;
                LoadInnings();
            }
        }

        public void LoadInnings()
        {
            title.Text = "<h5>Innings of " + GetInningsTeam(InningsId) + "</h5>";
            batters.InningsId = InningsId;
            bowlers.InningsId = InningsId;
        }

        private static string GetInningsTeam(int innings)
        {
            string result;
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetInningsTeam";

                cmd.Parameters.AddWithValue("@Innings", innings);
                var temp = cmd.Parameters.Add("@Result", SqlDbType.NVarChar);
                temp.Size = 100;
                temp.Direction = ParameterDirection.Output;
                var oversparam = cmd.Parameters.Add("@Overs", SqlDbType.Decimal);
                oversparam.Direction = ParameterDirection.Output;

                conn.Open();
                cmd.ExecuteScalar();

                result = cmd.Parameters["@Result"].Value + " (" + cmd.Parameters["@Overs"].Value + " overs)";
                
                conn.Close();
            }
            return result;
        }
    }
}