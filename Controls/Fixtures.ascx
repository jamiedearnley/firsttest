﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Fixtures.ascx.cs" Inherits="Scorecard.Fixtures" %>
<%@ Register TagPrefix="ss" TagName="ScoreSummary" Src="~/Controls/ScoreSummary.ascx" %>
<%@ Register TagPrefix="fh" TagName="FixtureHighlight" Src="~/Controls/FixtureHighlight.ascx" %>
<%@ Register TagPrefix="fu" TagName="FixtureURL" Src="~/Controls/FixtureURL.ascx" %>

<asp:SqlDataSource ID="sqlFixtures" runat="server" 
    ConnectionString="<%$ ConnectionStrings:Scorecard %>"   
    SelectCommand="GetFixtures" 
    SelectCommandType="StoredProcedure"
    OnSelecting="UpdateSqlParametersEventHandler">
    <SelectParameters>
        <asp:Parameter Name="Club" Type="String" />
        <asp:Parameter Name="Team" Type="String" />
        <asp:Parameter Name="Year" Type="DateTime" />
    </SelectParameters>
</asp:SqlDataSource>
<div id="fixturectrl">
 <asp:Literal runat="server" ID="title"></asp:Literal>
 <asp:Literal runat="server" ID="prevFixtures"></asp:Literal>
 <asp:Literal runat="server" ID="nextFixtures"></asp:Literal>

<asp:Repeater
         runat="server"
         id="FixturesGrid"
         DataSourceID="sqlFixtures">
        <HeaderTemplate>
 <table class="fixtures">
  <tr class="blankrow">
   <th align="left" class="fixturedate">DATE</th>
   <th align="right" class="fixtureteam">HOME TEAM</th>
   <th>&nbsp;</th>
   <th align="left" class="fixtureteam">AWAY TEAM</th>
   <th align="left" class="fixturecomp">COMPETITION</th>
   <th>&nbsp;</th>
   <th align="left">SPONSOR</th>
  </tr>
        </HeaderTemplate>
        <ItemTemplate>
  <fh:FixtureHighlight ID="fxhighlight" Fixture='<%# Eval("ID")%>' runat="server" />
   <td align="left" class="fixturedate"><time datetime="<%# Eval("MatchDate")%>"><%# Eval("MatchDay")%></time></td>
   <td align="right" class="fixtureteam"><%# Eval("HomeTeam")%> <ss:ScoreSummary ID="homesummary" Fixture='<%# Eval("ID")%>' Team='<%# Eval("HomeTeamID")%>' runat="server" /></td>
   <td align="center" width="30px">vs</td>
   <td align="left" class="fixtureteam"><ss:ScoreSummary ID="awaysummary" Fixture='<%# Eval("ID")%>' Team='<%# Eval("AwayTeamID")%>' runat="server" /> <%# Eval("AwayTeam")%></td>
   <td align="left"class="fixturecomp"><%# Eval("Competition")%> <%# Eval("Round")%></td>
   <td align="left"><fu:FixtureURL ID="fxurl" Fixture='<%# Eval("ID")%>' runat="server" /></td>
   <td align="left"><%# Eval("Sponser")%></td>
  </tr>
        </ItemTemplate>
        <FooterTemplate>
 </table>
        </FooterTemplate>
 </asp:Repeater>
 <script type="text/javascript">
     $("tr:not(.blankrow)").click(function () {
         window.location.href = $(this).find("a").attr("href");
     });

     $("tr:not(.blankrow)").hover(function () {
         $(this).addClass("rowhover");
         $(this).attr("title", $(this).find("a").attr("title"));
     },
         function () {
             $(this).removeClass("rowhover");
         });
 </script>
</div>