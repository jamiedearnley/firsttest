﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FixtureSummary.ascx.cs" Inherits="Scorecard.FixtureSummary" %>

<asp:SqlDataSource ID="sqlFixtures" runat="server" 
           ConnectionString="<%$ ConnectionStrings:Scorecard %>"   
           SelectCommand="FixtureDate" 
           SelectCommandType="StoredProcedure"
           OnSelecting="UpdateSqlParametersEventHandler">
           <SelectParameters>
            <asp:Parameter Name="Club" Type="String" />
            <asp:Parameter Name="Date" Type="DateTime" />
           </SelectParameters>
      </asp:SqlDataSource>
      
      <asp:Repeater
         runat="server"
         id="MenuItems"
         DataSourceID="sqlFixtures">
         <ItemTemplate>
           <p class="tk-ff-enzo-web"><%# Eval("HomeClub")%> <%# Eval("HomeTeam")%> vs <%# Eval("AwayClub")%> <%# Eval("AwayTeam")%> - <%# Eval("Competition")%> <%# Eval("Round")%></p>
         </ItemTemplate>
      </asp:Repeater>