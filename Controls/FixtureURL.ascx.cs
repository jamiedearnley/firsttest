﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Scorecard.Controls
{
    public partial class FixtureUrl : System.Web.UI.UserControl
    {
        public string Fixture
        {
            get { return hiddenFixture.Value; }
            set
            {
                hiddenFixture.Value = value;
                ControlLoaded();
            }
        }
        
        private void ControlLoaded()
        {
            if (!string.IsNullOrEmpty(hiddenFixture.Value))
            {
                var result = GetResult(Convert.ToInt32(hiddenFixture.Value));
                url.Text = !string.IsNullOrEmpty(result) ? "<a href='/Fixture/" + hiddenFixture.Value + "' title='" + result + "'></a>" : "&nbsp;";
            }
        }

        private static string GetResult(int fixture)
        {
            string result;
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "FixtureResult";

                cmd.Parameters.AddWithValue("@Fixture", fixture);
                var temp = cmd.Parameters.Add("@Result", SqlDbType.NVarChar);
                temp.Size = 100;
                temp.Direction = ParameterDirection.Output;
                conn.Open();
                cmd.ExecuteScalar();
                result = cmd.Parameters["@Result"].Value as string;
                conn.Close();
            }

            return result;
        }
    }
}