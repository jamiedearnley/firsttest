﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InningsBowlers.ascx.cs" Inherits="Scorecard.Controls.InningsBowlers" %>

<asp:SqlDataSource ID="sqlInningsBowlers" runat="server" 
    ConnectionString="<%$ ConnectionStrings:Scorecard %>"   
    SelectCommand="GetInningsBowlers" 
    SelectCommandType="StoredProcedure"
    OnSelecting="UpdateSqlParametersEventHandler">
    <SelectParameters>
        <asp:Parameter Name="Innings" Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>

<div id="bowlers">
    <asp:Repeater
         runat="server"
         id="BattersGrid"
         DataSourceID="sqlInningsBowlers">
        <HeaderTemplate>
            <table class="inningsbowlers">
            <tr class="bowlerheader">
                <th align="left" class="tk-ff-enzo-web">Bowling</th>
                <th align="left" width="50" class="tk-ff-enzo-web">O</th>
                <th align="left" width="50" class="tk-ff-enzo-web">M</th>
                <th align="left" width="50" class="tk-ff-enzo-web">R</th>
                <th align="left" width="50" class="tk-ff-enzo-web">W</th>
                <th align="right" width="40" class="tk-ff-enzo-web">Econ</th>
            </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td align="left" class="tk-ff-enzo-web"><%# Eval("Bowler")%></td>
                <td align="left" width="50" class="tk-ff-enzo-web"><%# Eval("Overs")%></td>
                <td align="left" width="50" class="tk-ff-enzo-web"><%# Eval("Maidens")%></td>
                <td align="left" width="50" class="tk-ff-enzo-web"><%# Eval("Runs")%></td>
                <td align="left" width="50" class="tk-ff-enzo-web"><%# Eval("Wickets")%></td>
                <td align="right" width="40" class="tk-ff-enzo-web"><%# Eval("Econ")%></td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
     </asp:Repeater>
</div>