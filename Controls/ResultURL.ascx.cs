﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;

namespace Scorecard.Controls
{
    public partial class ResultUrl : UserControl
    {
        public string Result
        {
            get { return hiddenResult.Value; }
            set
            {
                hiddenResult.Value = value;
                ControlLoaded();
            }
        }

        private void ControlLoaded()
        {
            if (!string.IsNullOrEmpty(hiddenResult.Value))
            {
                var result = GetResult(Convert.ToInt32(hiddenResult.Value));
                url.Text = !string.IsNullOrEmpty(result) ? "<a href='/Fixture/" + hiddenResult.Value + "' title='" + result + "'>" + GetResultText(Convert.ToInt32(hiddenResult.Value)) + "</a>" : "&nbsp;";
            }
        }

        private string GetResultText(int fixture)
        {
            string result;
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "FixtureResultText";

                cmd.Parameters.AddWithValue("@Fixture", fixture);
                var temp = cmd.Parameters.Add("@Result", SqlDbType.NVarChar);
                temp.Size = 150;
                temp.Direction = ParameterDirection.Output;
                conn.Open();
                cmd.ExecuteScalar();
                result = cmd.Parameters["@Result"].Value as string;
                conn.Close();
            }

            return result;
        }

        private static string GetResult(int fixture)
        {
            string result;
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "FixtureResult";

                cmd.Parameters.AddWithValue("@Fixture", fixture);
                var temp = cmd.Parameters.Add("@Result", SqlDbType.NVarChar);
                temp.Size = 100;
                temp.Direction = ParameterDirection.Output;
                conn.Open();
                cmd.ExecuteScalar();
                result = cmd.Parameters["@Result"].Value as string;
                conn.Close();
            }

            return result;
        }
    }
}