﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Newtonsoft.Json;

namespace Scorecard.Controls
{
    class AuthDetails
    {
        public byte[] Hash { get; set; }
        public string Salt { get; set; }
        public bool HasPermissions { get; set; }
    }
    
    public partial class Authentication : System.Web.UI.UserControl
    {
        public string Permissions
        {
            get { return hiddenFlags.Value; }
            set
            {
                hiddenFlags.Value = value;
            }
        }
        
        public bool Authenticate()
        {
            var result = false;
            if (Page.IsValid)
            {
                var flags = hiddenFlags.Value;
                if (AccountExists(emailaddress.Text))
                {
                    var details = GetAccountDetails(emailaddress.Text, flags);
                    if (ConfirmPassword(password.Text, details.Salt, details.Hash))
                    {
                        if (details.HasPermissions)
                        {
                            result = true;
                        }
                        else
                        {
                            SendEmail(emailaddress.Text, flags);
                            ltrAuthError.Text = "<p class=\"autherror tk-ff-enzo-web\">Permission denied.</p>";
                        }
                    }
                    else
                    {
                        ltrAuthError.Text = "<p class=\"autherror tk-ff-enzo-web\">Password incorrect. <a href=\"/PasswordResetRequest/" + HttpUtility.UrlEncode(emailaddress.Text) + "\">Forgotten password?</a></p>";
                    }
                }
                else
                {
                    ltrAuthError.Text = "<p class=\"autherror tk-ff-enzo-web\">email address not found. Please <a href=\"/Register/\">Register</a></p>";
                }
            }
            return result;
        }

        private static bool AccountExists(string email)
        {
            bool result;
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "FindAccount";

                cmd.Parameters.AddWithValue("@Email", email);
                var accountexists = cmd.Parameters.Add("@Result", SqlDbType.Bit);
                accountexists.Direction = ParameterDirection.Output;
                conn.Open();
                cmd.ExecuteScalar();
                result = Convert.ToBoolean(cmd.Parameters["@Result"].Value);

                conn.Close();
            }

            return result;

        }

        private static AuthDetails GetAccountDetails(string email, string flags)
        {
            AuthDetails result;
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetAccountDetails";

                cmd.Parameters.AddWithValue("@Email", email);
                cmd.Parameters.AddWithValue("@Flags", flags);
                var hash = cmd.Parameters.Add("@Hash", SqlDbType.VarBinary);
                hash.Size = 4000;
                hash.Direction = ParameterDirection.Output;
                var salt = cmd.Parameters.Add("@Salt", SqlDbType.NVarChar);
                salt.Size = 255;
                salt.Direction = ParameterDirection.Output;
                var permissions = cmd.Parameters.Add("@HasPermissions", SqlDbType.Bit);
                permissions.Direction = ParameterDirection.Output;
                conn.Open();
                cmd.ExecuteScalar();
                result = new AuthDetails
                    {
                        HasPermissions = Convert.ToBoolean(cmd.Parameters["@HasPermissions"].Value),
                        Salt = (string)cmd.Parameters["@Salt"].Value,
                        Hash = (byte[]) cmd.Parameters["@Hash"].Value
                    };

                conn.Close();
            }

            return result;

        }

        public static byte[] Hash(string value, string salt)
        {
            return Hash(Encoding.UTF8.GetBytes(value), Encoding.UTF8.GetBytes(salt));
        }

        public static byte[] Hash(byte[] value, byte[] salt)
        {
            byte[] saltedValue = value.Concat(salt).ToArray();

            return new SHA256Managed().ComputeHash(saltedValue);
        }

        public bool ConfirmPassword(string pw, string salt, byte[] hash)
        {
            byte[] passwordHash = Hash(pw, salt);

            return hash.SequenceEqual(passwordHash);
        }


        private static void SendEmail(string email, string flags)
        {
            var httpWReq = (HttpWebRequest)WebRequest.Create("http://api.postmarkapp.com/email");

            var encoding = new ASCIIEncoding();
            var emailbody =
                "the user: " + email + " just failed to authenticate as the permission (" + flags + ") was missing from their account";
            var postdata = new Dictionary<string, string>
                {
                    {"From", "jamie.dearnley@shepleycc.com"},
                    {"To", "jamie.dearnley@shepleycc.com"},
                    {"Subject", "Shepley CC permission denied warning"},
                    {"Tag", "Permissions"},
                    {"TextBody", emailbody}
                };
            byte[] data = encoding.GetBytes(JsonConvert.SerializeObject(postdata));

            httpWReq.Method = "POST";
            httpWReq.Headers = new WebHeaderCollection{"X-Postmark-Server-Token: aa0f7d43-6878-4326-9920-ecfc53764bdf"};
            httpWReq.ContentType = "application/json";
            httpWReq.ContentLength = data.Length;

            using (var newStream = httpWReq.GetRequestStream())
            {
                newStream.Write(data,0,data.Length);
            }

        }
    }
}