﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Web.UI;

namespace Scorecard.Controls
{
    public partial class ScoreSummary : UserControl
    {
        public string Team
        {
            get { return hiddenTeam.Value; }
            set 
            { 
                hiddenTeam.Value = value;
                ControlLoaded();
            }
        }
        public string Fixture
        {
            get { return hiddenFixture.Value; }
            set
            {
                hiddenFixture.Value = value;
                ControlLoaded();
            }
        }


        public void ControlLoaded()
        {
            if (!string.IsNullOrEmpty(hiddenTeam.Value) && !string.IsNullOrEmpty(hiddenFixture.Value))
            {
                var inningsId = GetInnings(Convert.ToInt32(Fixture), Convert.ToInt32(Team));
                summary.Text = inningsId != -1 ? GetSummary(inningsId) : CheckAbandonment(Convert.ToInt32(Fixture));
            }
        }

        private static string CheckAbandonment(int fixture)
        {
            string result = "";
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "CheckAbandonment";

                cmd.Parameters.AddWithValue("@Fixture", fixture);
                var abandoned = cmd.Parameters.Add("@Result", SqlDbType.Bit);
                abandoned.Direction = ParameterDirection.Output;
                conn.Open();
                cmd.ExecuteScalar();
                if (Convert.ToBoolean(cmd.Parameters["@Result"].Value))
                {
                    result = "(-)";
                }
                conn.Close();
            }

            return result;
        }
        private static int GetInnings(int fixture, int team)
        {
            int result;
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "FindInnings";

                cmd.Parameters.AddWithValue("@Fixture", fixture);
                cmd.Parameters.AddWithValue("@Team", team);
                var id = cmd.Parameters.Add("@ID", SqlDbType.Int);
                id.Direction = ParameterDirection.Output;
                conn.Open();
                cmd.ExecuteScalar();
                result = (int) cmd.Parameters["@Id"].Value;
                conn.Close();
            }

            return result;
        }

        private static string GetSummary(int innings)
        {
            string result;
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "InningsSummary";

                cmd.Parameters.AddWithValue("@Innings", innings);
                var wickets = cmd.Parameters.Add("@Wickets", SqlDbType.Int);
                wickets.Direction = ParameterDirection.Output;
                var runs = cmd.Parameters.Add("@Runs", SqlDbType.Int);
                runs.Direction = ParameterDirection.Output;
                conn.Open();
                cmd.ExecuteScalar();
                result = "(" + cmd.Parameters["@Runs"].Value + "/" + cmd.Parameters["@Wickets"].Value + ")";
                conn.Close();
            }

            return result;
        }
    }
}