﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Scorecard
{
    public partial class PasswordReset : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var token = Request.QueryString["token"];
            if (!string.IsNullOrEmpty(token))
            {
                hiddenToken.Value = token;
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (UpdatePassword(hiddenToken.Value))
                {
                    ltrResult.Text = "<p class=\"tk-ff-enzo-web\">Your password has been updated</p>";
                }
            }
        }

        private bool UpdatePassword(string token)
        {
            bool result;
            var salt = CreateSalt();
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "UpdatePassword";

                cmd.Parameters.AddWithValue("@Hash", Hash(password.Text, salt));
                cmd.Parameters.AddWithValue("@Salt", salt);
                cmd.Parameters.AddWithValue("@Token", token);
                var updated = cmd.Parameters.Add("@Result", SqlDbType.Bit);
                updated.Direction = ParameterDirection.Output;
                conn.Open();
                cmd.ExecuteScalar();
                result = Convert.ToBoolean(cmd.Parameters["@Result"].Value);
                conn.Close();
            }
            return result;
        }

        public static byte[] Hash(string value, string salt)
        {
            return Hash(Encoding.UTF8.GetBytes(value), Encoding.UTF8.GetBytes(salt));
        }

        public static byte[] Hash(byte[] value, byte[] salt)
        {
            byte[] saltedValue = value.Concat(salt).ToArray();

            return new SHA256Managed().ComputeHash(saltedValue);
        }

        private string CreateSalt()
        {
            var rng = new RNGCryptoServiceProvider();
            var buff = new byte[10];
            rng.GetBytes(buff);

            return Convert.ToBase64String(buff);
        }
    }
}