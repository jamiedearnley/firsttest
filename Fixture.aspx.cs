﻿using System;

namespace Scorecard
{
    public partial class Fixture : System.Web.UI.Page
    {
        private int? _id;

        public int FixtureId
        {
            get
            {
                if (_id == null)
                {
                    _id = -1;
                }
                return Convert.ToInt32(_id);
            }
            set { _id = value; }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            var id = Request.QueryString["ID"];
            if (!string.IsNullOrEmpty(id))
            {
                int fixtureId;
                if (int.TryParse(id, out fixtureId))
                {
                    FixtureId = fixtureId;
                    scorecard.FixtureId = FixtureId;
                }
            }
        }
    }
}