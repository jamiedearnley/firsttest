﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Scorecard
{
    public partial class JuniorCricket : System.Web.UI.Page
    {
        private DateTime? _year;
        public DateTime Year
        {
            get
            {
                try
                {
                    if (_year == null)
                    {
                        _year = DateTime.Now;
                    }
                    return Convert.ToDateTime(_year);
                }
                catch (Exception)
                {
                    return DateTime.Now;
                }
            }
            set { _year = value; }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            u15Fixtures.Year = Year;
            u13Fixtures.Year = Year;
            u11Fixtures.Year = Year;
        }
    }
}