﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="History.aspx.cs" Inherits="Scorecard.History" %>
<%@ Register TagPrefix="Nav" TagName="Navigation" Src="~/Controls/Navigation.ascx" %>
<%@ Register TagPrefix="Ftr" TagName="Footer" Src="~/Controls/Footer.ascx" %>
<!DOCTYPE html>
<html lang="en">
 <head>
  <meta charset="utf-8" />
  <meta name="description" content="Shepley cricket club" />
  <meta name="author" content="Jamie Dearnley" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0" />
  <link rel="stylesheet" media="screen, projection" href="/styles/screen.css" />
  <title>Shepley Cricket Club - History</title>
  
  <!-- enable HTML5 elements in IE7+8 -->
  <!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <script type="text/javascript" src="//use.typekit.net/mdu4awa.js"></script>
  <script type="text/javascript">      try { Typekit.load(); } catch (e) { }</script>
  <script type="text/javascript" src="/scripts/jquery-1.8.3.min.js"></script>
 </head>
 <body>
  <div id="container">
   <form runat="server">
    <Nav:Navigation runat="server" ID="pageNav" />
   <article id="history" class="first large">
	<h1>HISTORY OF THE CLUB</h1>
	<div class="inlinepost">
	 <p>At a meeting held at the Black Bull Inn, Shepley, on July 21st 1871 it was resolved &quot;That we have a Cricket Club to be called &lsquo;The Shepley Cricket Club&rsquo; and that we have Laithe Croft (now Cliffe House) of Mr Senior&prime;s, subject to conditions named, and the rent to be five pounds a year to be paid in advance&quot;.</p>
	 <p>This historical fact records the beginning of the Club and at its formation the officers for 1871 were:</p>
	 <p>
	  <ul>
	   <li>Mr Edward Armitage, <em>President</em></li>
	   <li>Mr Geo. Barden, <em>Vice-President</em></li>
	   <li>Mr Jackson Armitage, <em>Secretary</em></li>
	   <li>Mr William Smith, <em>Assistant Secretary</em></li>
	  </ul>
	 </p>
	 <p>Little is known of the actual cricket played in the inaugural year but we do know that the closing match was played on Saturday, 14 October 1871, and a minute states &quot;the match to commence at 2 o&prime;clock and that we have a substantial supper to be on the table at 6 o&prime;clock&quot;.</p>
	 <p>The first Annual Meeting was held after the supper and arrangements were made to have the ground laid but the expenditure was not to exceed fifteen pounds.  The square was to be 40 by 30 yards, to be lowered 4 inches on the end near the road and raised six inches at the other end.  The work was to be done by one Robert Drake and it had to be finished in a workmanlike manner to the satisfaction of the members, at 16/- per 100 square yards.  No extras were to be allowed unless previously agreed to by the Committee and no extras to be allowed if the sods were got in the field for the deficient places.  All day work was to be charged at 3/10d per day.</p>
	 <p>Some of the first fixtures were against St John&prime;s, Ecclesfield, Shelley, Lockwood Mechanics Institute and Northgate House.</p>
	 <p>While there is no record of cricket scores in 1871 the accounts of that year show that 2 bats were purchased for 1 guinea, a ball for 4/6, 2 pairs of pads for 18/6, wicket keeping gloves for 7/- and a cricket net complete for &pound;1.12.6, &pound;2 was paid for rent of the field and Robert Drake received &pound;11.10s for ground laying.</p>
	 <p>In 1872 on 29th July, Allen Roebuck, Henry Ellis, John Battye, Sam Bower, James Beaumont, Sam Smith and Isaac Hardy were admitted to play the season out for 1/- each subject to the conditions that they behaved themselves to the satisfaction of the Committee.</p>
	 <p>Apparently in these early days of the Club, football was also played on the ground, as well as quoits and brasses and no member was allowed to join for quoits separately.</p>
	 <p>R Crooks was the first professional to be engaged by the Club in 1874 and he received 35/- per week.</p>
	 <p>Matches in that year were played against Kirkheaton, Hallamshire Wanderers and Sheffield Wednesday but a challenge by Meltham was &quot;left over until Dr. Douglas enquires what kind of Club it is&quot;.  Lascelles Hall were also invited to play at Shepley that year, as well as Wortley, Oughty Bridge, Attercliffe and Penistone.</p>
	 <p>On 6th May 1874 it was resolved for practices that</p>
	 <p>
	  <dl>
	   <dt>No player be allowed to <em>bat</em> until he has <em>longstopped</em>.</dt>
	   <dt>No player be allowed to bat more than <em>seven</em> minutes at once.</dt>
	   <dt>No player be allowed to leave the field (after batting) until he has fielded for, and seen 2 men out.</dt>
	  </dl>
	 </p>
	 <p>On 25th January 1875 in Committee it was resolved that &quot;With respect to the Yorkshire United County Club wishing to arrange for a 2 or 3 days match on our ground, we write to ask them upon what terms they will come and also send us names of a few of the probable players&quot;.</p>
	 <p>Trinity Feast time was always a special gala occasion for the Club.  Often a 2 day match was played and Yorkshire County players were guests.  Tennis was introduced while croquet, quoits and brasses were popular.  More than one Band was engaged and there were athletic events by day and entertainment at night.  It was a time of general festivity with the Shepley Cricket Club playing a leading part.</p>
	 <p>The Club prospered until 1888 when it is understood that notice was given to quit the Laithe Croft ground as James Senior of the Brewery wished to build Cliffe House on the site.  The affairs of the Club were wound up and a sale of equipment, advertised in the Yorkshire Post on February 11th, 13th and 14th was undertaken by a Mr. Wilby.</p>
	 <p>A new Club was formed almost immediately with Mr. W. H. Senior as President, Mr. W. Bradley as Treasurer and Mr. J. L. Noble as Secretary.  Probably for 2 or possibly 3 years the Club played adjacent to the old Stag Inn but very soon moved to its present site which was also loaned by the Brewery.</p>
	 <p>There seems to be no positive information about the Club until 1901 when Fred Sykes a fast left hand bowler was professional.  That year Shepley played Lepton Highlanders in the final of the Combination Cup at Thurstonland.  Shepley batted first scoring 80 and when Lepton were seven down for 30 a message arrived in Shepley, possibly by pigeon, that Shepley must surely be Cup Winners that day.  The Shepley Brass Band set off to meet the victors but alas when it reached Stones Wood Bottom the news was that Lepton&prime;s last two had knocked &prime;em off.  Tears were shed and the Band turned about and returned to Shepley without playing.</p>
	 <p>Shepley joined the Dearne Valley League in 1912 and finished Champions.  Herbert Kirk was professional and scored the fastest century of the season against Cawthorne.</p>
	 <p>Harry Holden was Captain of Shepley at that time and Benny Ellis, now 88 and living at Bird&rsquo;s Edge was in the team.  Hayden Matthews was Secretary.</p>
	 <p>1913 was also a vintage year when the Club won the Combination Cup Final and also the 2nd Team Championship.</p>
	 <p>There was no League Cricket during the 1914 -1918 War.  George Herbert Hirst however did present the trophies in 1914 when he advised Herbert Hunter to &quot;Throw &prime;em well up to bat lad&quot;.  Friendlies were played in wartime and many people do remember playing tennis, croquet, quoits and brasses (pork pie ones).</p>
	 <p>The Club restarted soon after the War and joined the Central League in 1920.  Alderman T. G. Holden, now 87, was Captain, Frank Wood was Secretary, Herbert Kirk was groundsman and also played.</p>
	 <p>Shepley were Central League Champions in 1925 and the team was:</p>
	 <p>
	  <ul>
	   <li>T.G. Holden, <em>Captain</em></li>
	   <li>W. Beever, <em>Vice-Captain</em></li>
	   <li>Harold Moorhouse, <em>Wicketkeeper</em></li>
	   <li>Arthur Noble</li>
	   <li>Arthur Blacker</li>
	   <li>Rawson Bingham</li>
	   <li>L.V. Roebuck</li>
	   <li>Norman Holroyd</li>
	   <li>F.P. Woodhead</li>
	   <li>W. Henry Moorhouse</li>
	   <li>Friend Allsop, <em>Professional</em></li>
	   <li>John Ely, <em>12th Man</em></li>
	  </ul>
	 </p>
	 <p>The Final was against Penistone at Netherfield.  Rawson Bingham did not bat having broken his leg and Harold Moorhouse was injured early on and William Henry Moorhouse was substitute wicketkeeper</p>
	 <p>1925 was a good season for Shepley C.C. as they also reached the final of the Tinker Cup when they played but lost to Cumberworth United.</p>
	 <p>The Club then had a few lean years, when admittedly, there was a decline in playing ability, finances were critically low, a successful time was not enjoyed.  However, as often happens, youth took over and the Club managed to carry on and in 1930 the 2nd XI won the Tinker Cup Final but afterwards there was an objection in favour of Lepton Highlanders as an ineligible player was included.</p>
	 <p>When Friarmere withdrew there was a vacancy in the Huddersfield League and Shepley applied, and it was a pleasant surprise to be accepted, in 1933 and at this time the Club certainly had possibilities and a decent ground.</p>
	 <p>The first match played in the Huddersfield League against Paddock was on April 15th, 1933 and the teams were:</p>
	 <p>
	  <table>
        <tr>
          <td>1st XI</td>
          <td>2nd XI</td>
        </tr>
        <tr>
          <td>J. Hindle</td>
          <td>N. Wortley</td>
        </tr>
        <tr>
          <td>R.H. Windle</td>
          <td>J. Hindle</td>
        </tr>
        <tr>
          <td>P. Bedford</td>
          <td>H.L. Mosley</td>
        </tr>
        <tr>
          <td>L.V. Roebuck</td>
          <td>J. Precious</td>
        </tr>
        <tr>
          <td>H. Wray</td>
          <td>W. Heywood</td>
        </tr>
        <tr>
          <td>F.Q. Woodhead (Pro)</td>
          <td>R. Haigh</td>
        </tr>
        <tr>
          <td>L. Bradley</td>
          <td>J. Wright</td>
        </tr>
        <tr>
          <td>R. Taylor</td>
          <td>L. Battye</td>
        </tr>
        <tr>
          <td>J.S. Precious</td>
          <td>N. Biltcliffe</td>
        </tr>
        <tr>
          <td>W. Reeds (Capt.)</td>
          <td>A. Lockwood</td>
        </tr>
        <tr>
          <td>S. Townsend</td>
          <td>A. Stockhill</td>
        </tr>
      </table>
	 </p>
	 <p>There was a new enthusiasm for the Club and the first Tea Hut was built in the same year for &pound;60.</p>
	 <p>1936 was a great playing year for Shepley when they won promotion to Division &lsquo;A&rsquo; of the Huddersfield League.  Unfortunately relegation took place the following year and then there was struggle for existence during the war years but the Club and Cricket in Shepley managed to keep going.</p>
	 <p>Things began picking up in 1946 and in the following year Shepley reached its first semi-final of the Sykes Cup although they were beaten by Lascelles Hall.</p>
	 <p>In 1948 the Club was awarded the Hopkinson Trophy.</p>
	 <p>The Club had many ups and downs and for the period 1945 &ndash; 1952 was greatly indebted to the late Bob Stewart who was Secretary.</p>
	 <p>Shepley had a better season in 1957 when George Waites was professional but were soon struggling again and often had to apply for re-election to the League.</p>
	 <p>That great character Leonard Battye also did valiant work for the Club in these lean years and was largely instrumental in keeping the Club going.  His humour and dialect never flagged and after he introduced and ran the Ground Improvement Scheme and the Forecast Draw the Club has never looked back financially.  He was League representative for 21 years and is now Chairman of the Club.</p>
	 <p>In 1961 electricity was installed in the tent and work began on new toilets.  In that year Joe Dransfield was Chairman, T.G.B. Jackson was President, Davis Woodhead was Captain and professional was Frank Squire.</p>
	 <p>At this time great efforts were made to improve the ground and amenities, so much so, that in 1962 the Greenwood Trophy was won and presented to George Jackson by R.E. Haigh, Esq.</p>
	 <p>The same Officers continued to serve the Club and in 1964 David Woodward became Secretary an office he still holds in our Centenary Year and Tommy Kaye took over the Captaincy.</p>
	 <p>1966 was a vintage year again for Shepley as the 1st XI, Captain Gordon Lancaster, won the Hinchliffe Cup and the 2nd XI, Captain John Stirk, won the Schweppes Cup.  In the same year Gordon Schofield won the League bowling prize, David Midgley the 1st XI League catching prize and J. Stirk the 2nd XI League catching prize.  Financially the Club was in a healthy position and a new mower was purchased at a cost of &pound;360.</p>
	 <p>The following year G.A. Keen, Esq., was elected President and the final contract for the purchase of the cricket field was signed on September 4th.  The trustees then elected were:</p>
	 <p>
	  <ul>
	   <li>George Washington, <em>Solicitor</em></li>
	   <li>Leonard Battye, <em>Textile Worker</em></li>
	   <li>Anthony Keen, <em>Schoolmaster</em></li>
	   <li>Tom Kaye, <em>Company Director</em></li>
	   <li>Paul Smelt, <em>Accountant</em></li>
	  </ul>
	 </p>
	 <p>Also during 1967 Brian Kettlewell was signed as professional, the tea room was extended and extensive repairs were done to the pavilion.  Although the Club were relegated this year, it was flourishing and in good heart.  Ian Stevenson won the League 2nd XI stumping prize.</p>
	 <p>In 1968 the same officers carried on except that John Holden was made 1st XI Captain and in 1969 Leonard Battye became Chairman of the Club and Paul Smelt took over the 2nd XI Captaincy.</p>
	 <p>In 1969 the profit from the forecast draw was &pound;742.  The final payment on the ground was settled and an epoch in the history of the Club was established when the Title Deeds were received by the Club from Bass Charrington Ltd on 6th March.  According to the minute records and the memory of Alderman T.G. Holden, the Brewery, first Seth Seniors, then Hammonds, and afterwards Bass Charringtons have in turn fostered the Club over the years.  James Senior helped found and launch the Club in 1871.  He himself played, was first Treasurer and very much a benefactor.  The Club has always played on Brewery land.  Initially on Laithe Croft, then by the Stag, afterwards just off Jenkyn Lane and finally the present ground which the Club now possesses freehold.  The Deeds of the ground were presented to the Club by the League President, A. Lodge Esq., and on behalf of the Club, Joe Dransfield received them during the interval of the match against Thongsbridge on 12th July before a good gathering.</p>
	 <p>The Club was now prospering again and work on the new tea room had been started.  Keith Davies became 1st XI Captain in 1970 in which year he hit a ton and agreement was reached with Almondbury Casuals re- the lease of the ground for Sunday matches.  In this year, Ken Fisher was elected Social Secretary for the Committee for the Centenary Season.  In that year Ian Stevenson won the League 2nd XI stumping prize with an all time record of 46 victims.</p>
	 <p>All members of the Committee and other stalwart members have worked hard for the Centenary programme and the ground is in good condition thanks to the ground staff and a licence to sell intoxicating liquor has been obtained.</p>
	 <p>Among the events in the provisional programme for the Centenary Year are:</p>
	 <p>
	  <table>
        <tr>
          <td>April 24th</td>
          <td>Opening of new Tea Hut</td>
        </tr>
        <tr>
          <td>April 29th</td>
          <td>Social Evening at British Legion Club</td>
        </tr>
        <tr>
          <td>June 20th</td>
          <td>Centenary Match v. Almondbury Casuals (Dress as 100 years ago).</td>
        </tr>
        <tr>
          <td>September 4th</td>
          <td>Roast and Sideshows on Ground</td>
        </tr>
        <tr>
          <td>September 5th</td>
          <td>Shepley CC v. &quot;Saints&quot;</td>
        </tr>
        <tr>
          <td>September 12th</td>
          <td>Double Wicket Competition</td>
        </tr>
        <tr>
          <td>September 24th</td>
          <td>Centenary Dinner, Painthorpe Country Club</td>
        </tr>
	  </table>
	 </p>
	 <p>Finally, may I thank all who have kindly helped in the writing of this History of Shepley Cricket Club.  It has been my pleasure to meet some very old members of the Club, the period before the 1914 &ndash;1918 War must have seen some great cricket in Shepley.  I would like to thank our Secretary, David Woodward for his help and to Leonard Battye for his guidance and company, he really is incorrigible and the Club owes a great deal to him.</p>
	 <p>To the Club, including the Ladies, may I wish you all a very happy and successful Centenary Season and I most sincerely hope that the Club, now owning its own ground, will prosper in every way in future years.</p>
	 <p>G. A. KEEN</p>
	 <p><em>President 1971</em></p>
	</div>
	<hr />
   </article>
   </form>
   <div class="clearfooter"></div>
  </div>
  
<Ftr:Footer runat="server" ID="pageFooter" />
 </body>
</html>