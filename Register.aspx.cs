﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;

namespace Scorecard
{
    public partial class Register : System.Web.UI.Page
    {

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (!AccountExists(emailaddress.Text))
                {
                    var token = CreateAccount(emailaddress.Text, password.Text);
                    SendEmail(emailaddress.Text, token.ToString());
                    ltrDebug.Text = "<p class=\"tk-ff-enzo-web\">Thank you for registering. A verification email has been sent to you</p>";
                }
                else
                {
                    ltrDebug.Text = "<p class=\"tk-ff-enzo-web\">This account is already registered</p>";
                }
                
            }
        }

        private static void SendEmail(string email, string token)
        {
            var httpWReq = (HttpWebRequest)WebRequest.Create("http://api.postmarkapp.com/email");

            var encoding = new ASCIIEncoding();
            var emailbody =
                "Thankyou for registering on the Shepley CC website\n\nPlease click this link to verify your account:\n\nhttp://www.shepleycc.com/Verify/" + token;
            var postdata = new Dictionary<string, string>
                {
                    {"From", "jamie.dearnley@shepleycc.com"},
                    {"To", email},
                    {"Subject", "Shepley CC registration confirmation"},
                    {"Tag", "Registration"},
                    {"TextBody", emailbody},
                    {"ReplyTo", "jamie.dearnley@shepleycc.com"}
                };
            byte[] data = encoding.GetBytes(JsonConvert.SerializeObject(postdata));

            httpWReq.Method = "POST";
            httpWReq.Headers = new WebHeaderCollection{"X-Postmark-Server-Token: aa0f7d43-6878-4326-9920-ecfc53764bdf"};
            httpWReq.ContentType = "application/json";
            httpWReq.ContentLength = data.Length;

            using (var newStream = httpWReq.GetRequestStream())
            {
                newStream.Write(data,0,data.Length);
            }

        }
        private Guid CreateAccount(string email, string pw)
        {
            var salt = CreateSalt();
            var token = Guid.NewGuid();
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "CreateAccount";

                cmd.Parameters.AddWithValue("@Email", email);
                cmd.Parameters.AddWithValue("@Hash", Hash(pw, salt));
                cmd.Parameters.AddWithValue("@Salt", salt);
                cmd.Parameters.AddWithValue("@Flags", "Read");
                cmd.Parameters.AddWithValue("@Token", token.ToString());
                var accountid = cmd.Parameters.Add("@ID", SqlDbType.Int);
                accountid.Direction = ParameterDirection.Output;
                conn.Open();
                cmd.ExecuteScalar();
                conn.Close();
            }
            return token;
        }

        private bool AccountExists(string email)
        {
            bool result;
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "FindAccount";

                cmd.Parameters.AddWithValue("@Email", email);
                var accountexists = cmd.Parameters.Add("@Result", SqlDbType.Bit);
                accountexists.Direction = ParameterDirection.Output;
                conn.Open();
                cmd.ExecuteScalar();
                result = Convert.ToBoolean(cmd.Parameters["@Result"].Value);
                
                conn.Close();
            }

            return result;

        }

        public static byte[] Hash(string value, string salt)
        {
            return Hash(Encoding.UTF8.GetBytes(value), Encoding.UTF8.GetBytes(salt));
        }

        public static byte[] Hash(byte[] value, byte[] salt)
        {
            byte[] saltedValue = value.Concat(salt).ToArray();

            return new SHA256Managed().ComputeHash(saltedValue);
        }

        private string CreateSalt()
        {
            var rng = new RNGCryptoServiceProvider();
            var buff = new byte[10];
            rng.GetBytes(buff);

            return Convert.ToBase64String(buff);
        }
    }
}