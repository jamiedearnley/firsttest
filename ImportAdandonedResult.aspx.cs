﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Web.UI.WebControls;

namespace Scorecard
{
    public partial class ImportAdandonedResult : System.Web.UI.Page
    {
        protected void btnAbandoned_Click(object sender, EventArgs e)
        {
            if (auth1.Authenticate())
            {
                var fixtures = (DropDownList) Page.FindControl("dlFixtures");
                if (fixtures != null)
                {
                    if (fixtures.SelectedIndex > -1)
                    {
                        using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
                        {
                            var cmd = conn.CreateCommand();
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "AbandonFixture";
                            cmd.Parameters.AddWithValue("@Fixture", fixtures.Items[fixtures.SelectedIndex].Value);
                            conn.Open();

                            cmd.ExecuteScalar();
                            conn.Close();
                        }
                        ltrDebug.Text = "Fixture marked as abandoned";
                    }
                    else
                    {
                        ltrDebug.Text = "You must select a fixture";
                    }
                }
            }
        }

        protected void btnHomeConceded_Click(object sender, EventArgs e)
        {
            if (auth1.Authenticate())
            {
                var fixtures = (DropDownList)Page.FindControl("dlFixtures");
                if (fixtures != null)
                {
                    if (fixtures.SelectedIndex > -1)
                    {
                        using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
                        {
                            var cmd = conn.CreateCommand();
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "ConcedeFixtureHomeTeam";
                            cmd.Parameters.AddWithValue("@Fixture", fixtures.Items[fixtures.SelectedIndex].Value);
                            conn.Open();

                            cmd.ExecuteScalar();
                            conn.Close();
                        }
                        ltrDebug.Text = "Fixture marked as conceded";
                    }
                    else
                    {
                        ltrDebug.Text = "You must select a fixture";
                    }
                }
            }
        }

        protected void btnAwayConceded_Click(object sender, EventArgs e)
        {
            if (auth1.Authenticate())
            {
                var fixtures = (DropDownList)Page.FindControl("dlFixtures");
                if (fixtures != null)
                {
                    if (fixtures.SelectedIndex > -1)
                    {
                        using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
                        {
                            var cmd = conn.CreateCommand();
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "ConcedeFixtureAwayTeam";
                            cmd.Parameters.AddWithValue("@Fixture", fixtures.Items[fixtures.SelectedIndex].Value);
                            conn.Open();

                            cmd.ExecuteScalar();
                            conn.Close();
                        }
                        ltrDebug.Text = "Fixture marked as conceded";
                    }
                    else
                    {
                        ltrDebug.Text = "You must select a fixture";
                    }
                }
            }
        }

        protected void dlTeams_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dlTeams.SelectedIndex != -1)
            {
                FindFixtureYears(dlTeams.SelectedItem.Value);
            }
        }

        protected void dlYears_SelectedIndexChanged(object sender, EventArgs e)
        {
            var dropDownList = sender as DropDownList;
            if (dropDownList != null)
                FindFixtures(dlTeams.SelectedItem.Value, dropDownList.SelectedItem.Value);
        }

        private void FindFixtureYears(string teamId)
        {
            int team;
            if (!int.TryParse(teamId, out team)) { throw new Exception("Unable to find years as teamid (" + teamId + ") is invalid"); }
            dlYears.Items.Clear();
            dlFixtures.Items.Clear();

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetFixtureYears";

                cmd.Parameters.AddWithValue("@Team", team);
                conn.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        dlYears.Items.Add(reader.GetInt32(0).ToString(CultureInfo.InvariantCulture));
                    }
                }

                FindFixtures(dlTeams.Items[dlTeams.SelectedIndex].Value, dlYears.Items[dlYears.SelectedIndex].Value);
                conn.Close();
            }
        }

        private void FindFixtures(string teamId, string y)
        {
            int team;
            int year;
            if (!int.TryParse(teamId, out team)) { throw new Exception("Unable to find fixtures as teamid (" + teamId + ") is invalid"); }
            if (!int.TryParse(y, out year)) { throw new Exception("Unable to find fixtures as year (" + y + ") is invalid"); }
            dlFixtures.Items.Clear();


            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["scorecard"].ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetFixturesByYearandTeam";

                cmd.Parameters.AddWithValue("@Team", team);
                cmd.Parameters.AddWithValue("@Year", year);
                conn.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        dlFixtures.Items.Add(new ListItem(reader.GetString(0), reader.GetInt32(1).ToString(CultureInfo.InvariantCulture)));
                    }
                }
                conn.Close();
            }
        }



        protected void dlTeams_DataBound(object sender, EventArgs e)
        {
            FindFixtureYears(dlTeams.SelectedItem.Value);
        }
    }
}