﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Sponsorship.aspx.cs" Inherits="Scorecard.Sponsorship" %>
<%@ Register TagPrefix="Nav" TagName="Navigation" Src="~/Controls/Navigation.ascx" %>
<%@ Register TagPrefix="Ftr" TagName="Footer" Src="~/Controls/Footer.ascx" %>
<!DOCTYPE html>
<html lang="en">
 <head>
  <meta charset="utf-8" />
  <meta name="description" content="Shepley cricket club" />
  <meta name="author" content="Jamie Dearnley" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0" />
  <link rel="stylesheet" media="screen, projection" href="/styles/screen.css" />
  <title>Shepley Cricket Club - About</title>
  
  <!-- enable HTML5 elements in IE7+8 -->
  <!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <script type="text/javascript" src="//use.typekit.net/mdu4awa.js"></script>
  <script type="text/javascript">      try { Typekit.load(); } catch (e) { }</script>
  <script type="text/javascript" src="/scripts/jquery-1.8.3.min.js"></script>
 </head>
 <body>
  <div id="container">
   <form runat="server">
    <Nav:Navigation runat="server" ID="pageNav" />
   
   <article id="whysponsor" class="first large">
	<h1>SPONSORSHIP: WHY?</h1>
	<div class="inlinepost">
	 <p>Founded in 1871, Shepley Cricket Club is at the heart of community life. A thriving and ambitious organisation, the Club welcomes the support of businesses and individuals in bringing sporting and social opportunities to the local population.</p>
	 <p>The Club&prime;s community activities include hosting the Shepley Spring Festival, a nationally important festival of music and traditional dance; the organisation of the Shepley Village Bonfire, the biggest such event for miles around; and being the venue of choice for family and other functions.</p>
	 <p>The Club welcomes 18,000 visitors each year:</p>
	 <p>
	  <ul>
	   <li>Shepley Spring Festival c10,000</li>
	   <li>Shepley Village Bonfire c3,000</li>
	   <li>Club and third-party Functions c1,500</li>
	   <li>Matches c2,000</li>
	   <li>Other Club activities c1500</li>
	  </ul>
	 </p>
	 <p>The Club website www.shepleycc.com attracts 14,000 visits per annum from 60 countries.</p>
	 <p>The Club is a community-based amateur sports club providing over 1500 coaching and playing opportunities to junior players each year, using qualified coaches and run entirely by volunteers. It is a non-profit making organisation.</p>
	 <p>The Club coaches children from the ages of seven to seventeen from Shepley and the surrounding villages.</p>
	 <p>The Club is embarked on a five year &pound;0.3m development programme to provide enhanced community and sporting facilities for the people of Shepley and its surrounding villages.</p>
	 <p>The Club has strong links within the local community, providing opportunities, use of facilities and financial support to a wide range of local organisations including Pre-School Groups, Shepley Cub Scouts, First Responders, Village Magazine, Shepley Band, Tennis Club, Rotary Club etc.</p>
	</div>
	<hr />
   </article>
   
   
   <article id="sponsorshipopportunities" class="large">
	<h1>SPONSORSHIP OPPORTUNITIES 2013</h1>
	<div class="inlinepost">
	 <p>Club sponsorship presents both individuals and companies with a low cost opportunity to be associated with a club playing top quality 1st and 2nd IX cricket in the prestigious Drakes Huddersfield League Premiership, together with five thriving junior sides.</p>
	 <p>Sponsorship payments are tax allowable for traders and companies and invoices can be provided.</p>
	 <p>Shepley Cricket Club offers a choice of sponsorship deals:</p>
	</div>
	<hr />
   </article>
   
   <article id="officals" class="large">
	<div class="split ls">
	 <h2>MATCH BALL SPONSORSHIP</h2>
	 <p>Sponsor&prime;s name prominently featured in the Clubhouse throughout the day of the match, and on two &lsquo;A&rsquo; boards in the village centre and outside the entrance to the ground. The Sponsor&prime;s name will appear on the front page of the Club website during the week leading up to the game, with a link to the sponsor&prime;s website where appropriate. Sponsor&prime;s name added to the list of sponsors displayed throughout the season in the Clubhouse and on the Club website.</p>
	 <p>Cost: &pound;45.00</p>
	 
	 
	 <h2>PERIMETER BOARD ADVERTISING</h2>
	 <p>Professionally produced 6&prime; x 2&prime; display board attached to the perimeter wall around the boundary edge and visible from all points of the ground. All board sponsors have a link from the Club&prime;s website to their own site.</p>
	 <p>Cost: &pound;100.00 per season. Setup cost &pound;100.00 + VAT (recoverable through business for advertising).</p>
	</div>
	
	<div class="split rs">
	 <h2>BENCH SPONSORSHIP</h2>
	 <p>A long-term sponsorship opportunity for a one-off cost, by funding the purchase of &lsquo;park-bench&rsquo; style seating at the ground. The eight-foot seat will be fitted with a brass plaque carrying your own wording and will be sited prominently in the spectator area. It will be maintained by the Club and will last for many years.</p>
	 <p>Cost: &pound;300.00</p>
	 
	 
	 <h2>SIMPLE DONATIONS</h2>
	 <p>A number of our benefactors simply make a donation to the Club at a level that suits their circumstances. These can be credited or anonymous and we are happy to discuss any requirements.</p>
	 <p>Cost: Any</p>
	</div>
	<hr />
   </article>
   </form>
   <div class="clearfooter"></div>
  </div>
  
<Ftr:Footer runat="server" ID="pageFooter" />
 </body>
</html>