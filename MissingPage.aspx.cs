﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Web.UI;
using Newtonsoft.Json;

namespace Scorecard
{
    public partial class MissingPage : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var url = Request.QueryString["URL"];
            if (!string.IsNullOrEmpty(url))
            {
                SendEmail(url);
            }
        }

        private static void SendEmail(string url)
        {
            var httpWReq = (HttpWebRequest)WebRequest.Create("http://api.postmarkapp.com/email");

            var encoding = new ASCIIEncoding();
            var emailbody =
                "The following page was just requested and resulted in a 404: (" + url + ")";
            var postdata = new Dictionary<string, string>
                {
                    {"From", "jamie.dearnley@shepleycc.com"},
                    {"To", "jamie.dearnley@shepleycc.com"},
                    {"Subject", "Shepley CC missing page request"},
                    {"Tag", "404"},
                    {"TextBody", emailbody}
                };
            byte[] data = encoding.GetBytes(JsonConvert.SerializeObject(postdata));

            httpWReq.Method = "POST";
            httpWReq.Headers = new WebHeaderCollection { "X-Postmark-Server-Token: aa0f7d43-6878-4326-9920-ecfc53764bdf" };
            httpWReq.ContentType = "application/json";
            httpWReq.ContentLength = data.Length;

            using (var newStream = httpWReq.GetRequestStream())
            {
                newStream.Write(data, 0, data.Length);
            }

        }
    }
}