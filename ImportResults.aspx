﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportResults.aspx.cs" Inherits="Scorecard.ImportResults" %>
<%@ Register TagPrefix="Nav" TagName="Navigation" Src="~/Controls/Navigation.ascx" %>
<%@ Register TagPrefix="Ftr" TagName="Footer" Src="~/Controls/Footer.ascx" %>
<%@ Register TagPrefix="Auth" TagName="Authentication" Src="~/Controls/Authentication.ascx" %>

<!DOCTYPE html>
<html lang="en">
 <head>
  <meta charset="utf-8" />
  <meta name="description" content="Shepley cricket club" />
  <meta name="author" content="Jamie Dearnley" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0" />
  <link rel="stylesheet" media="screen, projection" href="/styles/screen.css" />
  <title>Shepley Cricket Club - Import Results</title>
  
  <!-- enable HTML5 elements in IE7+8 -->
  <!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <script type="text/javascript" src="//use.typekit.net/mdu4awa.js"></script>
  <script type="text/javascript">      try { Typekit.load(); } catch (e) { }</script>
  <script type="text/javascript" src="/scripts/jquery-1.8.3.min.js"></script>
 </head>
 <body>
  <div id="container">
   <form id="form1" runat="server">
<Nav:Navigation runat="server" ID="pageNav" />
   <br />
    <div id="importresults">
     <Auth:Authentication runat="server" Permissions="Import" ID="auth1" />
     
     <h2>Import Results</h2>
     <fieldset>
      <ol>
       <li>The fixtures uploader accepts the standard drakes league excel spreadsheet. Please select a competition and if the fixture is a cup match also select a round. If the competition or round you require is not available please contact the system administrator.<br /><br /></li>
       <li>To import a result you must also provide your account details. If you do not have an account you will be asked to register after entering your details.<br /><br /></li>
       <li>If you wish to mark a fixture as abandoned or conceded please <a href="/ImportAdandonedResult.aspx">click here</a><br /><br /></li>
       <li><asp:DropDownList ID="dlCompetitions" runat="server" DataSourceID="sqlComps" DataTextField="Name" DataValueField="ID" ></asp:DropDownList>
           <asp:SqlDataSource ID="sqlComps" runat="server" ConnectionString="<%$ ConnectionStrings:Scorecard %>" SelectCommand="GetCompetitions" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
           <asp:DropDownList ID="dlRounds" runat="server" DataSourceID="sqlRounds" DataTextField="Name" DataValueField="ID" ></asp:DropDownList>
           <asp:SqlDataSource ID="sqlRounds" runat="server" ConnectionString="<%$ ConnectionStrings:Scorecard %>" SelectCommand="GetRounds" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
       </li>
       <li>
        <asp:FileUpload ID="FileUpload1" runat="server" />
        <asp:Button ID="btnImport" runat="server" Text="Import" onclick="btnImport_Click" />

       </li>
      </ol>
     </fieldset>
     
     
        
      <asp:Literal ID="ltrDebug" runat="server"></asp:Literal>
     </div>
    </form>
   
   <div class="clearfooter"></div>
  </div>
  
<Ftr:Footer runat="server" ID="pageFooter" />
 </body>
</html>