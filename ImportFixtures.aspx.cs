﻿using System;
using System.Globalization;
using OfficeOpenXml;


namespace Scorecard
{
    public partial class ImportFixtures : System.Web.UI.Page
    {
        protected void btnImport_Click(object sender, EventArgs e)
        {
            if (auth1.Authenticate())
            {
                if (dlTeams.SelectedIndex != -1)
                {
                    if (FileUpload1.HasFile)
                    {
                        try
                        {
                            if (FileUpload1.PostedFile.ContentType == "application/vnd.ms-excel" || FileUpload1.PostedFile.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                            {
                                var pck = new ExcelPackage(FileUpload1.FileContent);
                                var wk = pck.Workbook.Worksheets[1];

                                if (ValidateWorksheet(wk))
                                {
                                    var count = 2;
                                    var processing = true;
                                    while (processing)
                                    {
                                        var date = wk.Cells[count, 1].Value;
                                        if (date != null)
                                        {
                                            DateTime fixturedate;
                                            if (DateTime.TryParseExact(date.ToString(), "ddd' 'dd'-'MMM' 'yyyy' 'H':'mm", CultureInfo.CurrentCulture, DateTimeStyles.AdjustToUniversal, out fixturedate))
                                            {
                                                var round = wk.Cells[count, 5].Value ?? string.Empty;
                                                var home = wk.Cells[count, 2].Value ?? "Unknown";
                                                var away = wk.Cells[count, 3].Value ?? "Unknown";
                                                var fixture = new FixtureHelper
                                                {
                                                    Home = new Team { Club = new Club { Name = home.ToString() }, Name = dlTeams.SelectedItem.Text },
                                                    Away = new Team { Club = new Club { Name = away.ToString() }, Name = dlTeams.SelectedItem.Text },
                                                    Date = fixturedate,
                                                    Competition = wk.Cells[count, 4].Value.ToString(),
                                                    Round = round.ToString()
                                                };


                                                var result = fixture.Id;
                                                count++;
                                            }
                                            else
                                            {
                                                ltrDebug.Text = "Date " + date + " is not a valid date";
                                                processing = false;
                                            }
                                        }
                                        else
                                        {
                                            processing = false;
                                        }
                                    }

                                    ltrDebug.Text = (count - 1) + " fixtures imported";
                                }
                                else
                                {
                                    ltrDebug.Text = "The uploaded spreadsheet is not in the required format";
                                }
                            }
                            else
                            {
                                ltrDebug.Text = "Fixtures must be imported from an excel file";
                            }
                        }
                        catch (Exception error)
                        {
                            ltrDebug.Text = error.Message;
                        }
                    }
                    else
                    {
                        ltrDebug.Text = "You must select an excel file to import";
                    }
                }
                else
                {
                    ltrDebug.Text = "You must select a team to import the fixtures for";
                }
            }
        }

        private static bool ValidateWorksheet(ExcelWorksheet wk)
        {
            try
            {
                var result = (string)wk.Cells[1, 1].Value == "Date";
                if ((string)wk.Cells[1, 2].Value != "HomeTeam")
                {
                    result = false;
                }
                if ((string)wk.Cells[1, 3].Value != "AwayTeam")
                {
                    result = false;
                }
                if ((string)wk.Cells[1, 4].Value != "Competition")
                {
                    result = false;
                }
                if ((string)wk.Cells[1, 5].Value != "Round")
                {
                    result = false;
                }

                return result;
            }
            catch
            {
                return false;
            }
        }
    }
}